# Ranking Server #

### 진행도 ###

* Session 발급/인증 완성. 후에 key-value store와 연동하는 것도 좋을 듯.
* Stage XML 데이터 입력, 출력 구현 (POST /stages, GET /stages/{stage_id}/xml_data)
* Facebook OAuth 2.0, Koala를 통한 Graph API 연동 완료
* Restful Resource Routing 완료
* Restful API, 호출 규약 정함.
