class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_default_response_format
  respond_to :json
  def set_default_response_format
     request.format = :json
  end

  def valid_param(param)
    return true if param.to_s != "" and param != nil
    return false
  end

  protected
    def log_info(category, message)
      $query_log.log_info(category,"#{request.method}, #{request.fullpath}, #{request.remote_ip}, #{message}") #error, fatal, debug, warn
    end
    def log_error(category, message)
      $query_log.log_error(category,"#{request.method}, #{request.remote_ip}, #{message}")
    end
    def log_debug(category, message)
      $query_log.log_debug(category,"#{request.method}, #{request.remote_ip}, #{message}")
    end
    def log_time_start
      $query_log.log_time_start
    end
end
