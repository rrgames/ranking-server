class FriendsController < ApplicationController
  before_filter :set_user, only: [:index, :create, :update, :destroy]
  # GET /users/{user_id}/friends
  def index
    log_time_start
    @field = valid_param(params[:field]) ? params[:field] : "name, profile_image, id"
    @friends = User.select(@field).where(id: Friend.find_by_user_id(@user.id))
    render json: { "friends" => @friends }
    log_info("get", LogMessages::REST_INF_FINE_RESP )
  rescue ActiveRecord::StatementInvalid
    render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
    log_error("get", LogMessages::REST_ERR_BAD_FIELD )
  end

  # POST /users/{user_id}/friends
  # friend id => id
  def create
    log_time_start
    @result_data.each do |i|
      @friend = Friend.new(:my_id => @user.id, :friend_id => params[:friend_id])
      if @friend.save
        render status: 201, json: { "error_code" => 201, "message" => LogMessages::REST_INF_FINE_RESP }
        log_info("post", LogMessages::REST_INF_FINE_RESP)
      else
        render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_NO_FRIEND }
        log_error("post", LogMessages::REST_ERR_NO_FRIEND)
      end
    end
  end

  # PATCH /users/{user_id}/friends
  def update
    log_time_start
    result_data = @user.api.get_connections('me','friends')
    Friend.delete_all(conditions: { my_id: @user.id })
  	result_data.each do |data|
      Friend.new(:my_id => @user.id,
                :friend_id => data["id"]).save
  	end
  end

  # DELETE users/{id}/friends/{friend_id}
   def destroy
     log_time_start
    @friend = Friend.where(:my_id => @user.id).where(:friend_id => params[:target_id])
    if @friend != nil
      @friend.destroy
      log_info('get', LogMessages::REST_INF_FINE_RESP)
    else
      #친구를 못찾으면 에러.
      render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND }
      log_error('get', LogMessages::REST_ERR_NOT_FOUND)
      return
    end
   end

  private
    def set_user
      log_time_start
      @user = User.find(params[:user_id])
      if @user == nil
        #User를 못찾으면 에러.
        render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND }
        log_error('get', LogMessages::REST_ERR_NOT_FOUND)
        return
      end
      log_info('get', LogMessages::REST_INF_FINE_RESP)
    end
end
