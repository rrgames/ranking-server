class ItemsController < ApplicationController

protect_from_forgery with: :null_session

#이것은 특정 아이디의 전체 아이템 목록 조회이다.
#GET Users/user_id/items
def index
## this user finding is temporary method.
#이렇게 해도 '유저'를 잘 찾는다.
#@user = User.find(params[:user_id])
#render status: 200, json: {"status" => @user.name}
	render status: 200, json: {"items" => Item.where(user_id: params[:user_id])}
	rescue ActiveRecord::StatementInvalid
		render status: 404, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_NOT_FOUND}
end

# 아이템이 없거나 0개인 경우에 추가하기.
## 아이템이 빈번한 업데이트가 일어나지는 않을껏.

#아이템의 전략을 세워야 한다.
#해당 아이템을 생성하게끔 할 것인가?
#보통 아이템 사용의 전략은 어떻게 되는가?
# 이미 아이템 종류는 정해져있다.
# 1. 삽입하려는 아이템이 있으면 그것을 '업데이트'.
# 2. 없다면 삽입.
# 3. 첫 조회시에는 아이템의 레코드 자체가 없다.
# 그렇다면 처음에는 아이템을 '생성만' 해두도록 하자.
# 아이템을 '구매'하는 상황은 어떠한가.

#POST users/:user_id/items
# form :item_id, :quantity
# 이것은 아이템을 구매하는 상황이라고 해두자.
def create
#@user = User.find(params[:user_id])
@item = Item.find_by(item_id: params[:item_id], user_id: params[:user_id]) if valid_param(params[:item_id])
	if @item # 있다면... 더하는 편이 좋지 않을려나...
	@item.quantity += params[:quantity].to_i
	@item.save
	render status:200, json: {"error_code" => 200, "message" => "Updation Complete."}
	return
	else #없다면 만드는 것이 맞다.
@item = Item.new(:user_id => params[:user_id],
		:item_id=> params[:item_id],
		:quantity => params[:quantity])
	@item.save
	render status:200, json: {"error_code" => 200, "message" => LogMessages::REST_INF_FINE_RESP}
	end
	end

## 이건 CRUD로 생각했을 경우를 가정한 것이다.
## PUT/PATCH users/:user_id/items/:id
## form data -> quantity
	def update
	@item = Item.find_by(item_id: params[:id], user_id: params[:user_id])
if(!@item)
	render status:404, json: {"error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND}
	return
	end
	@item.quantity = params[:quantity].to_i
	@item.save
	render status:200, json: {"error_code" => 200, "message" => LogMessages::REST_INF_FINE_RESP}
	end

## CRUD로 생각하지 말고,
## 어떤 상황이 있을지 생각해보자.
## 1. 아이디를 생성한 다음 처음으로 아이템이란 요소를 써먹어보려고 할때.
## 2. 아이템을 하나 사용함으로써 한개를 차감할때.

## GET users/:user_id/items/:item_id use
	def use
## 있으면 한개를 차감한다.
## 없으면 없다고 응답한다.
	@item = Item.find_by(item_id: params[:item_id], user_id: params[:user_id]) if valid_param(params[:item_id])
if(@item)
# 있으면 한개를 차감하자.
if(@item.quantity > 0)
	@item.quantity -= 1
	@item.save
	else
	render json: {"items" => "Current item is run out."}
	end

	end
	render json: {"items" => @item} # 아마도 NULL을 뿜겠지.

	end

#이것은 바로 본인찾기.
	def validate_authorization_for_user
@user = User.find_by_session_token(params[:token])
	if @user == nil
# 인증이 안돼서 에러.
	render status: 401, json: { "error_code" => 401, "message" => LogMessages::REST_ERR_BAD_TOKEN }
	end
	end

	end
