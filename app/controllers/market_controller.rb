class MarketController < ApplicationController
# 이것은 마켓을 컨트롤 하는 것이다. 헴헴
	before_filter :validate_authorization_for_user
	protect_from_forgery with: :null_session
 	# 필요한 기능을 여기에 명시하자.
	# 1. 어떤 상품을 팔고 있는지 조회.
	# 2. 개별 상품 디테일 보여주기.
	# 3. 구매 요청.
	# 4. 쪼(캐시) 충전
	# 5. 핑(머니) 충전
	def buy
		##필요한것 - 템 ID, 수량, 사용자 ID
		##사용자 아이디는 토큰으로 인증한 정보를 가져오자.
		##테스트니까 사용자 아이디는 걍 파라미터로 받아오는걸로.
		@goods = Goods.find(params[:item_id])
		#price_jjo * quantity;
		quantity = params[:quantity].to_i
		if @user.money_jjo < @goods.price_jjo * quantity ||
			@user.money_ping < @goods.price_ping * quantity
			render status: 400, json: { "error_code" => 401, "message" => "you have no money"}
			return
		else
			@user.money_jjo = @user.money_jjo - (@goods.price_jjo* quantity)
			@user.money_ping = @user.money_ping - (@goods.price_ping * quantity)
			@user.save
			@item = Item.find_by(item_id: @goods.id, user_id: @user.id)
			if @item
				@item.quantity += quantity
				@item.save
			else
				@item = Item.new(:user_id => @user.id,
						:item_id => @goods.id,
						:quantity => quantity)
				@item.save
			end
			render status: 200, json: { "error_code" => 201, "message" => "purchased!"}
			return	
		end
		rescue ActiveRecord::StatementInvalid
			render status:400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
		rescue ActiveRecord::RecordNotFound
			render status:400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_NOT_FOUND }
	end

	def show_banner
		@banner = Banner.all
		render status: 200, json: { "flashText" => "길어져도 잘 대응해서 마치 물 흐르듯이 옆으로 싹 밀려 들어오는 좋은 행사 배너란 이런거지", "banner" => @banner }
	end

	def show_goods
		if valid_param(params[:category])
			render json: { "goods" => Goods.where(category: params[:category])}
			return
		end
		if valid_param(params[:id])
			render json: { "goods" => Goods.find_by(id: params[:id])}
			return
		end
		@goods = Goods.all
		render status: 200, json: { "goods" => @goods}
	end

	def charge_cash
		@user.money_jjo = params[:money_jjo].to_i
		@user.money_ping = params[:money_ping].to_i
		@user.save	
		render status: 200, json: { "message" => "thank you for your purchasing!"}
	end

	def update_console
		if params[:kind] == 'goods'
			@goods = Goods.new(:name => params[:name],
							:desc => params[:desc],
							:category => params[:category],
							:price_jjo => params[:price_jjo].to_i,
							:price_ping => params[:price_ping].to_i,
							:discount => 0,
							:buy_count => 0)
			@goods.save
		end
		if params[:kind] == 'banner'
			@banner = Banner.new(:title => params[:title],
					:desc => params[:desc],
					:image => params[:image],
					:action => params[:act],
					:x => params[:x].to_i,
					:y => params[:y].to_i,
					:w => params[:w].to_i,
					:h => params[:h].to_i)
			@banner.save
		end
		render status: 200, json: { "error_code" => 401, "message" => "it works. just fine."}
	end

	def consume
		# needed : itemid, quantity, used_id
		render status: 200, json: { "error_code" => 200, "message" => LogMessages::REST_INF_FINE_RESP}
	end

	def validate_authorization_for_user
		@user = User.find_by_session_token(params[:token])
		if @user == nil
			# 인증이 안돼서 에러.
			render status: 401, json: { "error_code" => 401, "message" => LogMessages::REST_ERR_BAD_TOKEN }
		end
	end
end
