class RecordsController < ApplicationController
	before_filter :validate_authorization_for_user, only: [:create, :index]
	protect_from_forgery with: :null_session

	# POST stages/{stage_id}/records?post_sns=false&top_rank=top_rank
	def create
		log_time_start
##GA Start
		uri = URI.parse("http://www.google-analytics.com/collect")
		http = Net::HTTP::new(uri.host, uri.port)
		request = Net::HTTP::Post.new(uri.request_uri)
		request.set_form_data({"v"=> "1",
						"tid" => "UA-55619482-1",
						"cid" => @session.uuid,
						"t" => "pageview",
						"dp" => "record/"})
		http.request(request)
##GA END
		@stage = Stage.find_by(file_name: params[:stage_id]) if valid_param(params[:stage_id])
		@post_sns = valid_param(params[:post_sns]) ? true : false
		@top_rank = valid_param(params[:top_rank]) ? true : false

		if @stage
			@record = Record.where(user_id: @user.id).where(stage_id: @stage.id).first
			@record = Record.new if not @record

			@record.played_game_time = Time.now
			@record.play_time = params[:play_time]
			@record.score = params[:score]
			@record.played_turns = params[:played_turns]
			@record.user_id = @user.id
			@record.stage_id = @stage.id

			if @top_rank
				@user.top_rank = @stage.file_name
				@user.save
			end

			if @post_sns
				options = {
				  :message => "api, test, test, test"
					#:picture => ""
				}
				@user.api.put_object("me", "feed", options)
			end
			if @record.save
				render status: 201, json: { "record" => @record }
			end
			log_info("post", LogMessages::REST_INF_FINE_RESP)
		else
			# 스테이지를 못찾아서 에러.
			render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND }
			log_error("post", LogMessages::REST_ERR_NOT_FOUND)
		end
	rescue ActiveRecord::StatementInvalid
			# 필드가 잘못돼서 에러.
			render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
			log_error("post", LogMessages::REST_ERR_BAD_FIELD)
	end

	# GET stgaes/{stage_id}/me
	def me
		@records = Record.find_by(stage_id: params[:stage_id]).where(user_id: @user.id)
		if @records
			render status: 200, json: { "records"=> @records}
			return
		end
		render status: 400, json: {"error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND}	
	end

	# GET stages/{stage_id}/records
	#
	# params[:location]
	def index
		log_time_start
		@limit = valid_param(params[:limit]) ? params[:limit] : 10
		@field = valid_param(params[:field]) ? params[:field] : "score, played_turns, play_time, user_id"
		@stage = Stage.find_by(file_name: params[:stage_id]) if valid_param(params[:stage_id])
		if @stage
			@records = Record.select(@field).where(stage_id: @stage.id)
	 		if @records
				if valid_param(params[:location])
					@friends = Friend.find_by_user_id(params[:location])
					@friends << params[:location]
					@records = @records.where(user_id: @friends)
				end
				if valid_param(params[:uid])
					@records = @records.where(user_id: params[:uid])
				end
				render json: { "records" => @records.limit(@limit) }
				log_info("get", "stage record find finished")
			else
				#레코드를 못찾아서 에러.
				render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND + ": Record" }
				log_error("get", LogMessages::REST_ERR_NOT_FOUND )
			end
		else
			# 스테이지를 못찾아서 에러.
			render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND + ": Stage" }
			log_error("get", LogMessages::REST_ERR_NOT_FOUND )
		end
	rescue ActiveRecord::StatementInvalid
		# 필드가 잘못돼서 에러.
		render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
		log_error("get", LogMessages::REST_ERR_BAD_FIELD )
	end

	private

	def validate_authorization_for_user
		@user = User.find_by_session_token(params[:token])
		@session = Session.find_by_session_token(params[:token])
		if @user == nil
			# 인증이 안돼서 에러.
			render status: 401, json: { "error_code" => 401, "message" => LogMessages::REST_ERR_BAD_TOKEN }
		end
	end
end
