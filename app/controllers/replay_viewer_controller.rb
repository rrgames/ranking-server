class ReplayViewerController < ActionController::Base
  respond_to :html

  def index
    if valid_param(params[:rid]) and valid_param(params[:id])
      @rid = params[:rid]
      @id = params[:id]
      render
    else
      render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
      log_error('get', LogMessages::REST_ERR_BAD_FIELD)
    end
  end

private
  def valid_param(param)
    return true if param.to_s != "" and param != nil
    return false
  end
end
