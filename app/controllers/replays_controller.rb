class ReplaysController < ApplicationController
  before_filter :validate_authorization_for_user, only: [:update, :destory]
  protect_from_forgery with: :null_session

  # GET users/{user_id}/replays
  def index
    render json: { "replays" => Replay.all }
  end

  # GET users/{user_id}/replays/{id}
  def show
    render json: { "replay" => Replay.where(user_id: params[:user_id], id: params[:id]).first }
  end

  # POST users/{user_id}/replays
  def create
    @replay = Replay.new
    @replay.data = params[:data]
    @replay.user_id = params[:user_id]
    if @replay.save
      render json: { "replay" => @replay }
    else
      render json: { "error_code" => 500, "message" => "create fail..." }
    end
  end

  def destory

  end

private

  def validate_authorization_for_user
    @user = User.find(params[:user_id])
    if @user != User.find_by_session_token(params[:token])
      render status: 401, json: { "error_code" => 401, "message" => LogMessages::REST_ERR_BAD_AUTHO }
      log_error('get', LogMessages::REST_ERR_BAD_AUTHO)
    end
  end
end
