class SessionController < ApplicationController
	@@oauth_facebook_flash = Koala::Facebook::OAuth.new("1512783212267771", "3e6ac3f6ef903425006fd02a4aa3fd86", "https://rrgames.me:3000/client/")
	@@oauth_facebook_unity = Koala::Facebook::OAuth.new("1512783212267771", "3e6ac3f6ef903425006fd02a4aa3fd86", "https://rrgames.me:3000/client/unity.html")
	require 'digest/sha1'

	# GET /session/{code}
	def create
		log_time_start
		if params[:code]
			if params[:platform] == "flash"
		  	access_token = @@oauth_facebook_flash.get_access_token(params[:code])
			else
				access_token = params[:code]
			end
			graphApi = Koala::Facebook::API.new(access_token)
			facebook_profile = graphApi.get_object("me")
			user = User.find_by_facebook_uid(facebook_profile["id"])

			if (user == nil)
				user = User.new(
												:name => facebook_profile["name"],
												:gender => facebook_profile["gender"],
												:facebook_uid => facebook_profile["id"],
												:profile_image => graphApi.get_picture(facebook_profile["id"]),
												:money_ping => 0,
												:money_jjo => 0
												)
				user.save
			else
				user.profile_image = graphApi.get_picture(facebook_profile["id"])
				user.save
			end
			user.api = graphApi
			session = Session.new(
										:ip_addr => request.remote_ip.to_s,
										:login_time => Time.now,
										:user_id => user.id,
										:session_token => Digest::SHA1.hexdigest(Time.now.to_s + request.remote_ip.to_s + user.id.to_s),
										:uuid => SecureRandom.uuid
										)

			render json: { "session" => session }
			user.update_friend_list
			log_info('get', LogMessages::REST_INF_FINE_RESP)
		else
			render status: 409, json: { "error_code" => 409, "message" => LogMessages::REST_ERR_BAD_TOKEN}
			log_error('get', LogMessages::REST_ERR_BAD_TOKEN)
		end
	end
end
