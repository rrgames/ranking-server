class StagesController < ApplicationController
	protect_from_forgery with: :null_session

	# GET stages
	def index
		log_time_start
		if valid_param(params[:field])
			render json: { "stages" => Stage.all.select(params[:field])}
			log_info('get', LogMessages::REST_INF_FINE_RESP)
			return
		end
		render json: { "stages" => Stage.all}
		log_info('get', LogMessages::REST_INF_FINE_RESP)

	rescue ActiveRecord::StatementInvalid
		render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
		log_error('get', LogMessages::REST_ERR_BAD_FIELD)
	end

	# GET stages/{id}
	def show
		log_time_start
		if valid_param(params[:field])
			render json: { "stage" => Stage.select(params[:field]).find_by(file_name: params[:id])}
			log_info('get', LogMessages::REST_INF_FINE_RESP)
			return
		end
		render json: { "stage" => Stage.find_by(file_name: params[:id]) }
		log_info('get', LogMessages::REST_INF_FINE_RESP)

	rescue ActiveRecord::StatementInvalid
		render status: 400, json: { "error_code" => 400, "message" => LogMessage::REST_ERR_BAD_FIELD }
		log_error('get', LogMessages::REST_ERR_BAD_FIELD)
	end

	# POST stages
	# params { "stage_id" => id, "xml" => "XML TEXT DATA"}
	# 추후에 web-shell따위가 올라오지 않도록 보안에 신경을 써야함.
	def create
		# 이것은 파일을 하나 새로 생성해서 걍 디비에 넣어놓는 것이다.
		# 현재 아직은 전혀 디비를 건드리고 있지 않음
		log_time_start
		
		File.open("db/stageXMLs/" + params[:stage_id] + ".xml", "w+") do |f|
	  	f.write(params[:xml])
		end

		require "rexml/document"
		content = REXML::Document.new(params[:xml])

		@grade_one = content.elements["stage/grade/L1"].text.to_i
		@grade_two = content.elements["stage/grade/L2"].text.to_i
		@grade_three = content.elements["stage/grade/L3"].text.to_i

		content.elements.each("stage/missions/mission") do |elem|
			@rules = elem.text
		end

		@desc = content.elements["stage/description"].text
		require 'date'
		@stage = Stage.new(
						:file_name => params[:stage_id],
						:ruletype => @rules,
						:comment => @desc,
						:total_load => 0,
						:grade_one => @grade_one,
						:grade_two => @grade_two,
						:grade_three => @grade_three)
		@stage.save
		# 추후에 파일 검증하는 로직이 필요할것 같다.
		render json: { "message" => LogMessages::REST_INF_UPLOAD_OK}
		log_info('post', LogMessages::REST_INF_UPLOAD_OK )
	end

	#xml_simple이라고 하는 Gem을 써서 파씽을 하도록 하자.
	#This will be abandoned.

	# GET /stage_update/
	# 현재 이것은 임시적인환경이다.
	# 파일 내에 있는 것을 탐색하여 업데이트 할 것이다.
	# 그런데 사실은 이건 REST API로 남기는것보단
	# 그냥 루비 메서드로 따로 떼어놓는게 적절하다는 생각이 든다. 어째.

	def update
		#우선 리스트 업데이트를 위해 지금 있는 데이터들은 전부 삭제.
		log_time_start

		@stages = Stage.all;
		@stages.each do |s|
			s.destroy
		end
		isExist = 0
		10.times do |v|
			10.times do |i|
			#파일 이름을 정의해 놓아서 디비에 축적해놓도록 하자.
				filename = sprintf("db/stageXMLs/%d-%d.xml", v, i)
				if File.exist?(filename)
					afile = File.open(filename)
					#XML읽어서 일단 해시화 시켜놓자.
					require "rexml/document"
					content = REXML::Document.new(afile)
					#이것은 타겟 점수를 얻기 위함.

					@grade_one = content.elements["stage/grade/L1"].text.to_i
					@grade_two = content.elements["stage/grade/L2"].text.to_i
					@grade_three = content.elements["stage/grade/L3"].text.to_i

					content.elements.each("stage/missions/misson") do |element|
						@rules += element.text
					end

					@desc = content.elements["stage/description"].text

					isExist = isExist + 1
					@file_name = sprintf("%d-%d", v, i)
					@stage = Stage.new(:created_at => File.ctime(filename),
									:updated_at => File.mtime(filename),
									:file_name => @file_name,
									:ruletype => @rules,
									:comment => @desc,
									:total_load => 0,
									:grade_one => @grade_one,
									:grade_two => @grade_two,
									:grade_three => @grade_three)
					@stage.save
				end
			end
		end
		render json: { "message" => "%d file exists." % isExist }
		log_info('get', REST_INF_FINE_RESP)
	end

	# GET /stages/{stage_id}/xml_data
	def show_xml_data
		log_time_start
		if File.exist?("db/stageXMLs/" + params[:stage_id] + ".xml")
			render :file => "db/stageXMLs/" + params[:stage_id] + ".xml"
			log_info("get", LogMessages::REST_INF_FINE_RESP)
		else
			render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND }
			log_error("get", LogMessages::REST_ERR_NOT_FOUND)
		end
	end
end
