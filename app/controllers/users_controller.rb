class UsersController < ApplicationController
  before_filter :validate_authorization_for_user, only: [:update, :destory]

	# GET /users
	def index
    log_time_start
    if valid_param(params[:field])
			render json: { "users" => User.all.select(params[:field]) }
      log_info("get", LogMessages::REST_INF_FINE_RESP)
			return
		end
		render json: { "users" => User.all }
    log_info("get", LogMessages::REST_INF_FINE_RESP)
		#render status: 403, json: { "error_code" => 403, "message" => "접근할 수 없습니다." }
	uri = URI.parse("http://www.google-analytics.com/collect")
	http = Net::HTTP::new(uri.host, uri.port)
	request = Net::HTTP::Post.new(uri.request_uri)
	request.set_form_data({"v"=> "1",
						"tid" => "UA-55619482-1",
						"cid" => SecureRandom.uuid,
						"t" => "pageview",
						"dp" => "users/"})
	http.request(request)
	end

	# GET /users/{id}?field={field}
	def show
    log_time_start
		if valid_param(params[:field])
			render json: { "user" => User.select(params[:field]).find(params[:id]) }
      log_info("get", "User" + LogMessages::REST_INF_FINE_RESP)
			return
		end
		render json: { "user" => User.find(params[:id]) }
    log_info("get", "User" + LogMessages::REST_INF_FINE_RESP)
	uri = URI.parse("http://www.google-analytics.com/collect")
	http = Net::HTTP::new(uri.host, uri.port)
	request = Net::HTTP::Post.new(uri.request_uri)
	request.set_form_data({"v"=> "1",
						"tid" => "UA-55619482-1",
						"cid" => SecureRandom.uuid,
						"t" => "pageview",
						"dp" => "users/id"})
	http.request(request)

	rescue ActiveRecord::RecordNotFound
		render status: 404, json: { "error_code" => 404, "message" => LogMessages::REST_ERR_NOT_FOUND }
    log_error("get", "ERROR, " + LogMessages::REST_ERR_NOT_FOUND)
  rescue ActiveRecord::StatementInvalid
    render status: 400, json: { "error_code" => 400, "message" => LogMessages::REST_ERR_BAD_FIELD }
    log_error("get", "ERROR, " + LogMessages::REST_ERR_BAD_FIELD)
	end

	# POST /users
	# 인증과 동시에 만들어지므로 사용하지 않음.
	def create
		# @user = User.new
		# render json: { "params" => params.to_json }
	end

	# PUT/PATCH /users/{id}
	def update

	end

	# DELETE /users/{id}
	def destory

	end

private

 	def validate_authorization_for_user
    log_time_start
		@user = User.find(params[:id])
		if @user != User.find_by_session_token(params[:token])
			render status: 401, json: { "error_code" => 401, "message" => LogMessages::REST_ERR_BAD_AUTHO }
      log_error('get', LogMessages::REST_ERR_BAD_AUTHO)
      return
		end
    log_info('get', LogMessages::REST_INF_FINE_RESP)
	end
end
