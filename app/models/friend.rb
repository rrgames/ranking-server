class Friend
  include Mongoid::Document
  field :my_id, type: String
  field :friend_id, type: String

  def self.find_by_user_id(id)
    arr = []
    Friend.where(:my_id => id).each do |v|
      arr << v.friend_id
    end
    arr
  end
 end
