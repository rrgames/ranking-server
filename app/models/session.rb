class Session
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

	attr_accessor :login_time#, :datetime
	attr_accessor :ip_addr#, :string
	attr_accessor :user_id#, :integer
	attr_accessor :session_token#, :string
	attr_accessor :uuid #, :string-16byte.

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  class << self

   
    def new(*argu)
      @@sessions ||= {}
      session = super(*argu)
			@@sessions[session.session_token] = session
    end

		def find_by_session_token(token)
			return @@sessions[token]
		end
  end
end
