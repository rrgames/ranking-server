class User < ActiveRecord::Base
	# int id
	# string name
	# string profile_image
	# timestamp last_login_time
	# string current_login_ip
	# string session
	#
	#def create
	#end

	def self.find_by_facebook_uid(uid)
		find_by(
			facebook_uid: uid
		)
	end

	def self.find_by_session_token(token)
		session = Session.find_by_session_token(token)
		if session
			return find(session.user_id)
		end
		return nil
	end

	def update_friend_list
		Friend.any_in(my_id: self.id).destroy_all
 		@api.get_connections('me','friends').each do |data|
			friend = User.find_by_facebook_uid(data["id"])
			if friend
				Friend.new(my_id: self.id, friend_id: friend.id).save
			end
		end
	end

	def api=(api)
		@api = api
	end

end
