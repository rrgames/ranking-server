Rails.application.configure do
  # 캐싱할 것인가? 개발 모드에서는 항상 로드한다(캐시하지 않는다).
  config.cache_classes = false

  # Preloading 할 것인가?
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # 이것은 로그를 위해서 임이로 셋팅된 것임.

  #$globallogger = ActiveSupport::Logger.new('test.log')

  config.logger = ActiveSupport::Logger.new('log/myapp.log')

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
