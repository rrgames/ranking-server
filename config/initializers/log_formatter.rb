class ActiveSupport::Logger::SimpleFormatter
  CATEGORY_TAG_MAP     = {'DEBUG'=>'DBG', 'INFO'=>'INF', 'WARN'=>'WRN', 'ERROR'=>'ERR', 'FATAL'=>'FTL', 'UNKNOWN'=>'UKN'}

  #호출 함수를 재정의.
  def call(severity, time, progname, msg)
    form_category = sprintf("%-3s",CATEGORY_TAG_MAP[severity])
    form_time = time.strftime("%Y-%m-%d %H:%M:%S.") << time.usec.to_s[0..2].rjust(3)

    #아래의 내용이 리턴 값이 되는것 같다.
    #"\033[0;37m#{formatted_time}\033[0m [\033[#{color}m#{formatted_severity}\033[0m] #{msg.strip} (pid:#{$$})\n"
    #progname은 웬만하면 없구나.
    #포매팅은 이 이상 할수 있는게 없음.
    if msg.strip.empty?
      ""
    else
      "#{form_time} [#{form_category}] #{msg.strip}\n"
    end
  end
end
