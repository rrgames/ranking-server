class LogMessages
  # 나머지 응답이 FINISH 로 작용할 것이다.
  REST_ERR_GENE_FAIL = "ERROR : Generation Failed."
  REST_ERR_BAD_FIELD = "ERROR : Bad Field Given."
  REST_ERR_NOT_FOUND = "ERROR : Record Not Found."
  REST_ERR_BAD_TOKEN = "ERROR : Bad Token Error."
  REST_ERR_BAD_AUTHO = "ERROR : Authorization Failed."
  REST_ERR_NO_FRIEND = "ERROR : Friend Insertion Failed."

  REST_INF_FINE_RESP = "INFO : Well Respond."
  REST_INF_READ_FINE = "INFO : Record Reading Finished."
  REST_INF_UPLOAD_OK = "INFO : File Upload Success."
  REST_INF_FRIEND_OK = "INFO : Friend Relation Established."

end
