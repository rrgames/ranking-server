class QUERY_LOG
  @@tagged_log = ActiveSupport::TaggedLogging.new(Logger.new('log/specified.log'))
  @@tag_db_map = {"read" => "DB_READ", "write"=>"DB_WRITE", "delete"=>"DB_DELETE","update"=> "DB_UPDATE", "global"=>"GLOBAL" ,'get' => "RT_GET", 'post' => "RT_POST", 'patch' => "RT_PATCH", 'delete'=> "RT_DELETE"}

  @@time = Time.now

  # Log has error, debug, info, fatal, warn, unknown
  def initialize
    @@tagged_log.info "===========SERVER_GENERATED========== \n"
  end

  def log_info (category, message)
    msg_string = "#{time_finish}ms, #{message}, [#{caller[1]}]"
    @@tagged_log.tagged(@@tag_db_map[category]){
      @@tagged_log.info msg_string
    }
  end

  def log_debug (category, message)
    msg_string = "#{time_finish}ms, #{message}, [#{caller[1]}][#{caller[2]}]"
    @@tagged_log.tagged(@@tag_db_map[category]){
      @@tagged_log.debug msg_string
    }
  end

  def log_error (category, message)
    @elapsed = time_finish
    #caller[0]은 무조건 이 위치를 반환하기 때문에 유효하지 않다.
    msg_string = "#{time_finish}ms, #{message}, [#{caller[1]}, #{caller[2]}, #{caller[3]}]"
    @@tagged_log.tagged(@@tag_db_map[category]){
      @@tagged_log.error msg_string
    }
  end

  def log_warn (category, message)
    @elapsed = time_finish
    msg_string = "#{time_finish}ms, #{message}"
    @@tagged_log.tagged(@@tag_db_map[category]){
      @@tagged_log.warn msg_string
    }
  end

  def log_fatal (category, message)
    @elapsed = time_finish
    msg_string = "#{time_finish}ms, #{message}"
    @@tagged_log.tagged(@@tag_db_map[category]){
      @@tagged_log.fatal msg_string
    }
  end

  def log_time_start
    @@time = Time.now
  end

  private
  def time_finish
    elapsed = ((Time.now - @@time)*1000.0).to_i
    elapsed
  end
end

$query_log = QUERY_LOG.new
