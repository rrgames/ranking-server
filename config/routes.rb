Rails.application.routes.draw do
  get 'esh/index'

	resources :users do
		resources :friends, :only => [:index, :create, :destroy]
		resources :replays,  :only => [:index, :create, :show]
		resources :items, :only => [:index, :create, :update, :destroy] do
			get 'use' => 'items#use'
		end
		patch 'friends' => "friends#update"
	end
	# this is for temporary method to update current stage list.
#	get 'stage_update' => 'stages#update'

#	below things are managed for market controller
		
	post 'market/buy' => 'market#buy'
	get 'market/banner' => 'market#show_banner'
	get 'market/goods' => 'market#show_goods'
	post 'market/cash' => 'market#charge_cash'

	post 'market/update' => 'market#update_console'

	resources :stages, :only => [:index, :show, :create] do
		get 'xml_data' => 'stages#show_xml_data'
		resources :records, :only => [:create, :index]
	end
  post 'client', :to => redirect("/client")
  get 'session' => 'session#create'
	get 'replay_viewer' => 'replay_viewer#index'
end
