# int uid
# timestamp login_time
# string ip_addr
# string access_token

class AddColumnToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :facebook_uid, :integer
    add_column :sessions, :login_time, :datetime
    add_column :sessions, :ip_addr, :string
    add_column :sessions, :access_token, :string
  end
end
