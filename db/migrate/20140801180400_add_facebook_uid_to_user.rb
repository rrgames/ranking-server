class AddFacebookUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :facebook_uid, :integer
    remove_column :sessions, :facebook_uid
    remove_column :sessions, :access_token
    add_column :sessions, :session_token, :string
  end
end
