class AddColumnToRecord < ActiveRecord::Migration
  def change
    add_column :records, :user_id, :int
    add_column :records, :stage_id, :int
    add_column :records, :score, :int
    add_column :records, :played_turns, :int
    add_column :records, :play_time, :int
    add_column :records, :played_game_time, :datetime
  end
end
