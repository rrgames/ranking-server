class Add2ColumnToUser < ActiveRecord::Migration
  def change
   add_column :users, :profile_image, :string
   add_column :users, :email, :string
  end
end
