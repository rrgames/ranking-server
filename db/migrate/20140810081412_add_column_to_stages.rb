class AddColumnToStages < ActiveRecord::Migration
  def change
    # this is for add column stage name
    add_column :stages, :file_name, :string
  end
end
