class AddColumnsToStages < ActiveRecord::Migration
  def change
    add_column :stages, :ruletype, :string
    add_column :stages, :comment, :string
  end
end
