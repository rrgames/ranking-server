class ChangeStageTable4 < ActiveRecord::Migration
  def change
    #s.integer :grade_one, :grade_two, :grade_three, :total_load
    add_column :stages, :grade_one, :int
    add_column :stages, :grade_two, :int
    add_column :stages, :grade_three, :int
    add_column :stages, :total_load, :int
  end
end
