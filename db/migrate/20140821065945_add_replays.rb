class AddReplays < ActiveRecord::Migration
  def change
    create_table :replays do |t|

      t.timestamps
    end
    add_column :replays, :user_id, :int
    add_column :replays, :data, :string

  end
end
