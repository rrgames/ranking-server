class ResizeReplayData < ActiveRecord::Migration
  def change
        change_column :replays, :data, :text
  end
end
