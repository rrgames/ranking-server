class CreateEshes < ActiveRecord::Migration
  def change
    create_table :eshes do |t|
      t.string :peer

      t.timestamps
    end
  end
end
