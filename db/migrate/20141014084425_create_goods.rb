class CreateGoods < ActiveRecord::Migration
  def change
    create_table :goods do |t|
      t.string :name
      t.string :desc
      t.string :category
      t.integer :price_jjo
      t.integer :price_ping
      t.integer :buy_count
      t.integer :discount

      t.timestamps
    end
  end
end
