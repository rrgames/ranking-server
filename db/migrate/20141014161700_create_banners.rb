class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :title
      t.string :desc
      t.string :image
      t.string :action
      t.integer :x
      t.integer :y
      t.integer :w
      t.integer :h

      t.timestamps
    end
  end
end
