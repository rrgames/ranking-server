# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141014161700) do

  create_table "banners", force: true do |t|
    t.string   "title"
    t.string   "desc"
    t.string   "image"
    t.string   "action"
    t.integer  "x"
    t.integer  "y"
    t.integer  "w"
    t.integer  "h"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "eshes", force: true do |t|
    t.string   "peer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "goods", force: true do |t|
    t.string   "name"
    t.string   "desc"
    t.string   "category"
    t.integer  "price_jjo"
    t.integer  "price_ping"
    t.integer  "buy_count"
    t.integer  "discount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "records", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "stage_id"
    t.integer  "score"
    t.integer  "played_turns"
    t.integer  "play_time"
    t.datetime "played_game_time"
  end

  create_table "replays", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.text     "data"
  end

  create_table "stages", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_name"
    t.string   "ruletype"
    t.string   "comment"
    t.integer  "target_score"
    t.integer  "grade_one"
    t.integer  "grade_two"
    t.integer  "grade_three"
    t.integer  "total_load"
  end

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "facebook_uid",  limit: 8
    t.string   "name"
    t.string   "gender"
    t.string   "birthday"
    t.string   "profile_image"
    t.string   "email"
    t.string   "top_rank"
    t.integer  "money_ping",              default: 0
    t.integer  "money_jjo",               default: 0
  end

end
