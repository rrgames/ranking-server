var ani = {
	CON: {
		ITEM_LIST: ["패스", "끝말잇기", "찬스"," 점프", "뒤로", "한번더"]
	}
}

function aWrong(arg, repeat){
	displaySubject(arg, (repeat%2)?"normal":"aWrong");
	if(repeat > 0){
		ani.wt = setTimeout(aWrong, 300, arg, repeat-1);
	}else{
		displaySubject(my.room.game.subj.replace(/[^a-z]/gi, ""));
	}
}
function aAccept(arg){
	var tl;
	if(arg){
		tl = arg.length;
		GameSubjectPanel.innerHTML = "　";
		ani.wc = new Array();
		var WC = window.innerWidth*0.5-tl*8-10;
		var soundV = (tl>=10)?"WW":"W";

		for(var i=0; i<tl; i++){
			var D = document.createElement("label");
			D.className = "AChar";
			D.style.fontSize = "0px";
			D.innerHTML = arg.charAt(i);
			GameSubjectPanel.appendChild(D);
			var no = {
				id: i,
				left: WC+i*16,
				size: 36,
				alpha: 1,
				obj: D
			}
			D.style.left = no.left+"px";
			ani.wc.push(no);
			setTimeout(playSound, i*80, soundV, "W");
			setTimeout(_aAccept, i*80, no);
			setTimeout(aClean, 1600+i*50, no, tl-i > 3);
		}
	}else{
		tl = 0;
	}
	setTimeout(requestNextTurn, 1800+tl*50);
}
function _aAccept(o){
	if(o.size < 27){
		o.size = 24;
	}else{
		o.size += (24-o.size)*0.3;
		ani.at = setTimeout(_aAccept, 30, o);
	}
	o.obj.style.fontSize = Math.round(o.size)+"px";
}
function aClean(o, gone){
	var og = gone?0:(window.innerWidth*0.5-34+16*(o.id-ani.wc.length+3));
	if(o.left < 10){
		o.obj.style.display = "none";
	}else{
		o.left += (og-o.left)*0.5;
		ani.act = setTimeout(aClean, 30, o, gone);
	}
	o.obj.style.left = Math.round(o.left)+"px";
}
function aAppear(div){
	ani.aa = 0;
	div.style.opacity = ani.aa;
	ani.at = setTimeout(_aAppear, 30, div);
}
function _aAppear(div){
	ani.aa += 0.1;
	div.style.opacity = ani.aa;
	if(ani.aa >= 1){
		ani.aa = 1;
	}else{
		ani.at = setTimeout(_aAppear, 30, div);
	}
}
function aScore(arg){
	arg.alpha -= 0.014;
	arg.top -= (1-arg.alpha)*4;
	if(arg.alpha > 0){
		ani.st = setTimeout(aScore, 30, arg);
		arg.validateDiv();
	}else{
		arg.div.parentElement.removeChild(arg.div);
	}
}
function aStopBGM(){
	if(SC.bgm.volume > 0){
		SC.bgm.volume = Math.max(0, SC.bgm.volume-0.03);
		ani.sb = setTimeout(aStopBGM, 30);
	}else{
		SC.bgm.volume = 1;
		SC.bgm.pause();
	}
}
function aEarthquake(level){
	ani.eql = level;
	ani.eqx = 30;
	_aEarthquake(level);
}
function _aEarthquake(){
	ani.eqx += ani.eql*2;
	ani.eql *= -0.9;
	if(Math.abs(ani.eql) < 1){
		ani.eqx = 30;
	}else{
		ani.eqt = setTimeout(_aEarthquake, 80);
	}
	Middle.style.top = Math.round(ani.eqx)+"px";
}
function aResult(){
	ani.rby = -50;
	_aResult();
}
function _aResult(){
	ani.rby += (50-ani.rby)*0.3;
	GameResultBox.style.top = "calc("+Math.round(ani.rby)+"% - 180px)";
	ani.rbt = setTimeout(_aResult, 30);
}
function aItemEffect(item){
	clearInterval(ani.aet);
	ani.aey = 280;
	item.style.top = ani.aey+"px";
	_aItemEffect(item);
}
function _aItemEffect(item){
	ani.aey -= 1;
	item.style.top = Math.round(ani.aey)+"px";
	item.style.opacity = (ani.aey-180)*0.01;
	if(ani.aey > 0){
		ani.aet = setTimeout(_aItemEffect, 30, item);
	}else{
		item.parentElement.removeChild(item);
	}
}