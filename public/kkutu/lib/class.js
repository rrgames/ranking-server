function classify(obj){
	/* 쓰는 법
	obj
	└ _ARG:Array @생성자
	*/
	var R = obj._ARG;
	delete obj._ARG;
	for(var I in obj){
		R.prototype[I] = obj[I];
	}
	return R;
}

var Client = classify({
	_ARG: function(id, name){
		this.id = id;
		this.name = name;
	},

	id: null,
	name: null,
	ready: false
});
var Room = classify({
	_ARG: function(data){
		this.id = data.roomId || data.id;
		this.title = data.title;
		this.master = data.master;
		this.user = data.user;
		this.config = data.config;
	},

	id: null,
	title: null,
	master: null,
	user: null,
	config: null,

	getUserById: function(id){
		if(!this.user) return;
		var R;
		var l = this.user.length;
		for(var i=0; i<l; i++){
			if(this.user[i].id == id){
				R = this.user[i];
				break;
			}
		}
		return R;
	}
});
var UserDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "UserItem";
		this.validateDiv();
	},

	data: null,
	div: null,

	validateDiv: function(){
		var R = "";
		R += "<label class='ChatUserName'>"+this.data.name+"</label>";
		this.div.innerHTML = R;
	}
});
var RoomUserDiv = classify({
	_ARG: function(data, room){
		this.room = room;
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "RoomUserItem";
		this.validateDiv();
	},

	room: null,
	data: null,
	div: null,

	validateDiv: function(){
		var R = "";
		R += "<label class='RoomUserName'>"+this.data.name+"</label>";
		if(this.data.id == this.room.master){
			R += "<label style='color: #0000FF'>방장</label>";
		}else{
			if(this.data.ready){
				R += "<label style='color: #007700'>준비 완료!</label>";
			}else{
				R += "<label style='color: #888888'>준비 중…</label>";
			}
		}
		this.div.innerHTML = R;
	}
});
var GameUserDiv = classify({
	_ARG: function(data, room){
		this.room = room;
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "GameUserItem";
		this.validateDiv();
	},

	room: null,
	data: null,
	div: null,

	validateDiv: function(){
		try{
			user[this.data.id].item = this.data.game.item;
		}catch(e){
			return;
		}
		this.div.id = "GameUser_"+this.data.id;
		var R = "<div class='GameUserImage'></div>";
		R += "<div class='GameUserName'>"+this.data.name+"</div>";
		if(this.data.game.score != null){
			console.warn();
			var sv = this.data.game.score.toString();
			if(sv.charAt() == "-"){
				sv = "-00".substr(0, 5-sv.length)+sv.slice(1);
			}else{
				sv = "000".substr(0, 4-sv.length)+sv;
			}
			sv = sv.split("");
			sv.forEach(function(item, index, my){
				R += "<div class='GameUserScore'>"+item+"</div>";
			});
		}
		this.div.innerHTML = R;
	}
});
var ScoreDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.alpha = 1;
		this.left = 0;
		this.top = 0;
		this.div = document.createElement("div");
		this.div.className = "ScoreItem";
		this.validateDiv();
	},
	
	left: null,
	top: null,
	alpha: null,
	data: null,
	div: null,
	
	validateDiv: function(){
		this.div.style.left = this.left+"px";
		this.div.style.top = this.top+"px";
		this.div.style.opacity = this.alpha;
		
		var vs = Math.floor(10*this.data.word.length*this.data.usage);
		var R = "+"+(this.data.score-vs);
		R += "<br /><label style='font-size: 16px; color: #777777;'>희귀점수 +</label>"+vs;
		this.div.innerHTML = R;
	}
})
var RoomDiv = classify({
	_ARG: function(data, clickHandler){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "RoomItem";
		this.div.setAttribute("data-id", data.id);
		this.div.onclick = clickHandler;
		this.validateDiv();
	},

	data: null,
	div: null,

	validateDiv: function(){
		var R = "";
		R += "<label class='RoomTitle'>"+this.data.title+"</label><br />";
		R += this.data.user.length+"/"+this.data.config.userLimit+"명";
		this.div.innerHTML = R;
	}
});
var ChatDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "Chat";
		this.validateDiv();
	},

	data: null,
	div: null,

	validateDiv: function(){
		var R = "";
		switch(this.data.code){
			case USER_CONNECT:
			case ENTER:
				R += "<div class='ChatNoticeEnter'><label class='ChatUserName'>";
					R += this.data.data+"</label>님이 입장했습니다.";
				R += "</div>";
				break;
			case USER_DISCONNECT:
			case EXIT:
				R += "<div class='ChatNoticeExit'><label class='ChatUserName'>";
					R += this.data.data+"</label>님이 퇴장했습니다.";
				R += "</div>";
				break;
				break;
			case EXIT:
				break;
			case CHAT:
				R += "<div class='ChatNormal'><div class='ChatWriter ChatUserName'>";
					//R += "<img class='ChatImage' src='https://graph.facebook.com/"+this.data.client.fb.id+"/picture' width='16px' height='16px' />";
					R += this.data.client.name+"</div><div class='ChatContent'>"+this.data.data+"</div>";
				R += "</div>";
				break;
			default:
				console.warn("알 수 없는 유형:", this.data);
		}
		this.div.innerHTML = R;
	}
});
var MeaningDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "MeaningBlock";
		this.x = 0;
		this.div.style.left = this.x+"px";
		this.validateDiv();
	},

	data: null,
	x: null,
	div: null,

	moveX: function(val, animate){
		if(animate){
			this._aMoveX(val);
		}else{
			this.x = val;
			this.div.style.left = this.x+"px";
		}
	},
	_aMoveX: function(dest){
		var delta = dest-this.x;
		if(Math.abs(delta) < 1){
			this.x = dest;
		}else{
			this.x += delta*0.3;
			setTimeout(function(my){
				my._aMoveX(dest);
			}, 30, this);
		}
		this.div.style.left = Math.round(this.x)+"px";
	},
	validateDiv: function(){
		var R = "<label class='MeaningWord'>"+this.data.word+"</label><br />";
		R += "<label class='MeaningText'>"+this.data.mean+"</label>";
		this.div.innerHTML = R;
	}
});
var GameResultRankItem = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "GameResultRankItem";
		this.validateDiv();
	},
	
	data: null,
	div: null,
	
	validateDiv: function(){
		this.div.className = "GameResultRankItem"+((this.data.id == my.id)?" MyGRItem":"");
		var R = "<div class='GRRank'>"+this.data.rank+"</div>";
		R += "<div class='GRName'>"+user[this.data.id].name+"</div>";
		R += "<div class='GRScore'>"+this.data.score+"</div>";
		this.div.innerHTML = R;
	}
});
var ItemEffectDiv = classify({
	_ARG: function(index){
		this.index = index;
		this.div = document.createElement("div");
		this.div.className = "ItemEffect";
		this.validateDiv();
	},
	
	index: null,
	div: null,
	
	validateDiv: function(){
		var R = ani.CON.ITEM_LIST[this.index]+"!";
		this.div.innerHTML = R;
	}
});