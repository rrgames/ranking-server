class Client
	attr_accessor :id
	attr_accessor :name
	attr_accessor :socket
	attr_accessor :room
	attr_accessor :ready
	attr_accessor :game

	def initialize(id)
		@id = id
		@room = "lobby"
		@game = Game.new(self)
	end
	def enter(room)
		@room = room.id
		@ready = false
		room.users.push(self)
	end
	def exit()
		if @room == "lobby" then
			return
		end
		publish(@room, {
			:code => Con::EXIT,
			:id => @id
		})
		$rooms[@room].users.delete_if do |item|
			item.id == @id
		end
		# 방 인원이 0이 되어 방이 폭파되는 경우
		if $rooms[@room].users.length <= 0 then
			$rooms[@room].remove()
			$rooms.delete(@room)
			publish("global", {
				:type => "delete room",
				:code => Con::ROOM_DELETE,
				:roomId => @room
			})
		else
			$rooms[@room].publishData()
		end
		@room = "lobby"
	end
	def toObject()
		{
			:id => @id,
			:name => @name,
			:ready => @ready,
			:game => @game.toObject
		}
	end
end