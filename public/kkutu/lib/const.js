var WELCOME = "001";
var ERROR = "002";

var USER_CONNECT = "101";
var USER_DISCONNECT = "102";

var CHAT = "201";

var ENTER = "301";
var EXIT = "302";
var ROOM_CREATE = "303";
var ROOM_DELETE = "304";
var ROOM_UPDATE = "305";

var READY = "401";
var TRY_START = "402";
var START = "403";
var WARN = "404";
var GAME_UPDATE = "405";
var FINISH = "406";
var TRY_END = "407";
var ROUND_START = "411";
var TURN_START = "421";
var TURN_TIMEOUT = "422";
var TURN_PLAY = "423";
var ACCEPT = "424";
var WRONG_WORD = "425";

///////////////////////////////////////////////////////

var ITEM_NAME = ["패스", "끝말잇기", "찬스", "점프", "뒤로", "한번더"];