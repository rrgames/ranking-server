module Con

	C_C = ["b","c","d","f","g","h","l","m","n","p","r","s","t","v"]
	C_CL = 14
	C_V = ["a","e","e","e","i","o","u"]
	C_VL = 7

	# [0__] 시스템
	WELCOME = "001"
	ERROR = "002" # 서버의 오류

	# [1__] 통신
	USER_CONNECT = "101"
	USER_DISCONNECT = "102"
	
	# [2__] 채팅
	CHAT = "201"
	
	# [3__] 방
	ENTER = "301"
	EXIT = "302"
	ROOM_CREATE = "303"
	ROOM_DELETE = "304"
	ROOM_UPDATE = "305"
	
	# [4__] 게임
	READY = "401"
	TRY_START = "402"
	START = "403"
	WARN = "404" # 클라이언트의 오류
	GAME_UPDATE = "405"
	FINISH = "406"
	TRY_END = "407"
	ROUND_START = "411"
	TURN_START = "421"
	TURN_TIMEOUT = "422"
	TURN_PLAY = "423"
	ACCEPT = "424"
	WRONG_WORD = "425"

end
module Msg
	
	ALREADY_ACTIVATED = "이미 진행 중인 방입니다."
	NOT_YET_READIED = "다른 참가자들이 아직 준비되지 않았습니다."
	
end