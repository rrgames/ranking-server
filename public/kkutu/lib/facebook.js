window.fbAsyncInit = function() {
	Hider.style.display = "none";
	FB.init({
		appId      : '1479728205638536',
		xfbml      : true,
		version    : 'v2.1'
	});
	requestFBStatus();
};
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ko_KR/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function requestFBStatus(){
	FB.getLoginStatus(function(res){
		switch(res.status){
			case "connected":
				my.fb = {
					id: res.authResponse.userID
				}
				fbToken = res.authResponse.accessToken;
				requestFBInfo("me");
				hiderMsg();
				break;
			case "unknown":
			case "not_authorized":
				hiderMsg("FBLogin");
				break;
			default:
				console.warn("알 수 없는 유형: ", res.status);
		}
	});
}
function requestFBLogin(){
	FB.login(function(res){
		requestFBStatus();
	});
}
function requestFBInfo(id){
	if(id == "me"){	// 처음에만 불러오기 때문에 가능한 구현.
		FB.api("/me", function(res){
			my.fb.name = res.name;
			my.fb.gender = res.gender;
			connectSocketServer();
		});
	}
}