var isGaming = false;
var g;

function startGame(){
	RoomMenu.style.display = "none";
	GameMenu.style.display = "";
	RoomBox.style.display = "none";
	GameBox.style.display = "";
	isGaming = true;
	stopBGM(true);
	GameSubjectPanel.innerHTML = "잠시 후 시작합니다!";
	setTimeout(requestStartRound, 2000);
	g = {
		ef: setInterval(ef, 30),
		user: updateGameUser(),
		item: [0, 0, 0, 0, 0, 0],
		temScore: new Object()
	}
	room[my.room.id].user.forEach(function(item, index, my){
		g.temScore[item.id] = 0;
	});
	my.ready = true;
	requestToggleReady();
}
function closeGameBox(){
	RoomMenu.style.display = "";
	GameMenu.style.display = "none";
	RoomBox.style.display = "";
	GameBox.style.display = "none";
	GameResultBox.style.display = "none";
	isGaming = false;
	playBGM("lobby");
	requestEndGame();
	drawRoomUserList();
	clearInterval(g.ef);
}
function finishGame(rank){
	GameSubjectPanel.innerHTML = "게임 종료!";
	setTimeout(showResult, 2000, rank);
}
function showResult(rank){
	GameResultBox.style.display = "";
	GameResultItemBox.innerHTML = "";
	var myRank;
	rank.forEach(function(item, index, _my){
		if(item.id == my.id){
			myRank = index;
		}
		GameResultItemBox.appendChild(new GameResultRankItem(item).div);
	});
	GameResultTitle.innerHTML = (myRank+1)+"등";
	switch(myRank){
		case 0:
			GameResultTitle.style.color = "#FFFF00";
			break;
		case 1:
			GameResultTitle.style.color = "#CCCCCC";
			break;
		default:
			GameResultTitle.style.color = "#999999";
	}
	aResult();
}
function updateGameUser(){
	var R = new Array();
	GameUserBox.innerHTML = "";
	var l = my.room.user.length;
	for(var i=0; i<l; i++){
		var o = new GameUserDiv(my.room.user[i], my.room);
		R.push(o);
		GameUserBox.appendChild(o.div);
	}
	if(user[my.id].item){
		var il = document.getElementsByClassName("GameItemQuantity");
		for(i=0; i<6; i++){
			il[i].innerHTML = "x"+user[my.id].item[i];
		}
	}
	if(g){
		if(g.itemedUser){
			var puv = new ItemEffectDiv(g.itemedWhat);
			document.getElementById(g.itemedUser).appendChild(puv.div);
			aItemEffect(puv.div);
			delete g.itemedUser, g.itemedWhat;
		}
	}
	return R;
}
function startRound(round){
	g.total = 150000;
	g.ctt = g.total;
	g.round = round;
	GameRoundPanel.innerHTML = "Round "+round+"<label style='font-size: 12px; color: #AAAAAA;'>&nbsp;/"+room[my.room.id].config.round+"</label>";
	MeaningPanel.innerHTML = "";
	g.means = new Array();
	requestStartTurn();
}
function startTurn(data){
	my.room.game.subj = data.subj;
	displaySubject(data.subj.replace(/[^a-z]/gi, ""));
	var cti = my.room.user[data.turn].id;
	g.myTurn = cti == my.id;
	g.mtt = data.time;
	g.tt = g.mtt;
	g.turn = data.turn;
	g.timing = true;
	playSound("T"+((15000-data.time)/1400), "Turn");
	console.log("startTurn", data.time);
	if(g.timer != -1){
		clearTimeout(g.timer);
	}
	g.timer = setTimeout(requestTurnTimeout, data.time);
	var D = document.getElementById("GameUser_"+cti);
	D.className += " Pointed";
}
function displaySubject(arg, type){
	type = type || "start";
	GameSubjectPanel.innerHTML = arg;
	GameSubjectPanel.style.textDecoration = "inherit";
	switch(type){
		case "start":
			GameSubjectPanel.style.color = "#FFFF00";
			break;
		case "wrong":
			ani.wt = setTimeout(aWrong, 300, arg, 3);
		case "aWrong":
			GameSubjectPanel.style.textDecoration = "line-through";
			GameSubjectPanel.style.color = "#FF7777";
			break;
		case "end":
			aAccept(arg);
		case "normal":
			GameSubjectPanel.style.color = "#FFFFFF";
			break;
		default:
	}
}
function ef(){
	if(g.timing){
		g.ctt -= 30;
		g.tt -= 30;
		drawTimeBar();
	}
}
function accept(data){
	clearInterval(g.timer);
	g.timer = -1;
	g.timing = false;
	stopSound("Turn");
	toggleItem(-1);
	if(data.word){
		displaySubject(data.word, "end");
		var al = g.means.length;
		var v = new MeaningDiv(data);
		MeaningPanel.appendChild(v.div);
		v.div.className += " LatestWord";
		aAppear(v.div);
		if(data.word.length >= 10){
			aEarthquake(data.word.length*0.5);
		}
		g.means.push(v);
		for(var i=0; i<al; i++){
			g.means[i].div.className = "MeaningBlock";
			g.means[i].moveX(g.means[i].x+v.div.offsetWidth+4, true);
		}
		var psv = document.getElementsByClassName("GameUserItem Pointed")[0];
		var sv = new ScoreDiv(data);
		sv.left = psv.offsetLeft;
		sv.top = psv.offsetTop+50;
		sv.validateDiv();
		GameStage.appendChild(sv.div);
		aScore(sv);
		if(data.item != -1){
			g.itemedUser = psv.id;
			g.itemedWhat = data.item;
		}
	}else{
		aAccept(null);
		GameSubjectPanel.innerHTML = "패스!";
	}
	g.item.forEach(function(item, index, my){
		my[index] = 0;
	});
}
function wrong(data){
	displaySubject(data, "wrong");
	playSound("F", "F");
}
function drawTimeBar(){
	TotalTimeBar.style.width = "calc("+(g.ctt/g.total*100)+"% - 6px)";
	TotalTimeBar.innerHTML = Math.round(g.ctt*0.001)+"초";
	TurnTimeBar.style.width = "calc("+(g.tt/g.mtt*100)+"% - 6px)";
	TurnTimeBar.innerHTML = (g.tt*0.001).toFixed(1)+"초";
}
function drawTimeout(){
	g.timing = false;
	GameRoundPanel.innerHTML = my.room.user[g.turn].name+"ㅠㅠ";
	var D = document.getElementById("GameUser_"+my.room.user[g.turn].id);
	D.className += " BoomPointed";
	setTimeout(requestStartRound, 2700);
}
function updateGame(data){
	var l = data.user.length;
	my.room.game.subj = data.game.subj;
	for(var i=0; i<l; i++){
		var o = my.room.user[i].game;
		var p = g.user[i].data.game;
		g.temScore[g.user[i].data.id] = p.score;
		for(var I in data.user[i].game){
			o[I] = data.user[i].game[I];
			p[I] = o[I];
		}
	}
	updateGameUser();
}
function toggleItem(index){
	if(user[my.id].item[index] <= 0) return;
	var items = document.getElementsByClassName("GameItem");
	g.item.forEach(function(item, i, my){
		if(i == index){
			my[i] = my[i]?0:1;
			if(my[i]){
				if(index*(index-2) == 0){
					send();
				}
			}
		}else{
			my[i] = 0;
		}
		items[i].style.backgroundColor = my[i]?"rgba(150, 240, 180, 0.5)":"rgba(48, 150, 60, 0.5)";
	});
}