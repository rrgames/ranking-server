class Game
	# 게임 정보 및 게임 진행 상황을 담당
	attr_accessor :client
	attr_accessor :playing
	attr_accessor :score
	attr_accessor :toSent # 타임아웃 신호를 보냈는지 확인
	attr_accessor :item
	# item <Array> 내용
	#	:pass		0	자기 턴 넘기기
	#	:single		1	한 글자로 잇기
	#	:chance	2	컴퓨터가 대신 넘기기
	#	:jump		3	다음 한 사람 건너뛰기
	#	:back		4	진행 방향 반대로 하기
	#	:double	5	자기가 한 번 더 입력하기
	
	def initialize(client)
		@client = client
		@playing = false
		@item = [65536, 65536, 65536, 65536, 65536, 65536]
	end
	def start()
		@score = 0
		@client.ready = false
		@playing = true
	end
	def accept(data)
		@score += data[:score]
	end
	def boom()
		@score -= 200
	end
	def getItem(id)
		# 아이템은 { id = (턴을 끝내기 위해 소비한 시간)%6 } 인 것을 준다.
		# 순위 경기에서 이것이 정당할까?
		# -	순위 경기에서는 아이템을 못 쓰게 하거나
		# -	게임 시작 전 서로 공평하게 고르거나
		@item[id] += 1
	end
	def useItem(id)
		@item[id] -= 1
	end
	def flush(rank)
		# 게임 결과를 데이터베이스에 반영시키고, 반영 결과를 반환
		# 클라이언트는 게임이 끝난 것으로 간주
		# ※방의 게임은 끝나지 않았다.
		@playing = false
		res = {
			:id => @client.id,
			:score => @score,
			:rank => rank,
			:exp => getExp(@score),
			:ping => (@score*0.01).to_i
		}
		# 아직 데이터베이스가 없다!
		res
	end
	def toObject()
		{
			:score => @score,
			:item => @item
		}
	end
end