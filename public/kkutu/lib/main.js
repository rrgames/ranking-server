var my = new Object();
var fbToken;
var user = new Object();
var room = new Object();

function attachChat(data){
	ChatBox.appendChild(new ChatDiv(data).div);
	ChatBox.scrollTop = ChatBox.scrollHeight;
}
function drawUserList(){
	var uc = 0;
	UserBox.innerHTML = "";
	for(var I in user){
		uc++;
		UserBox.appendChild(new UserDiv(user[I]).div);
	}
	TopStatus.innerHTML = uc+"명 접속";
}
function drawRoomUserList(){
	RoomUserBox.innerHTML = "";
	for(var I in my.room.user){
		RoomUserBox.appendChild(new RoomUserDiv(my.room.user[I], my.room).div);
	}
}
function enterRoom(id){
	RoomBox.style.display = "none";
	LobbyMenu.style.display = "none";
	RoomMenu.style.display = "";
	MyRoom.style.display = "";
	ChatBox.innerHTML = "";
	TopStatus.innerHTML = "방에 들어가는 중";
	my.ready = false;
}
function exitRoom(){
	RoomBox.style.display = "";
	MyRoom.style.display = "none";
	RoomMenu.style.display = "none";
	LobbyMenu.style.display = "";
	ChatBox.innerHTML = "";
	TopStatus.innerHTML = "대기실";
}
function updateRoom(){
	RoomBox.innerHTML = "";
	for(var I in room){
		RoomBox.appendChild(new RoomDiv(room[I], requestEnterRoom).div);
	}
	if(my.room != null){
		TopStatus.innerHTML = "방 "+my.room.title;
		var master = my.room.master == my.id;
		StartBtn.style.display = master?"":"none";
		RSBtn.style.display = StartBtn.style.display;
		ReadyBtn.style.display = master?"none":"";
		(isGaming?updateGameUser:drawRoomUserList)();
	}
}
function dialog(type){
	if(!type){
		Dialog.style.display = "none";
		return;
	}
	Dialog.style.display = "";
	var v = document.getElementById("DG_"+type);
	if(!v) return;
	var s = [v.getAttribute("data-width"), v.getAttribute("data-height")];
	DialogTitle.innerHTML = v.getAttribute("data-title");
	DialogStage.innerHTML = v.innerHTML;
	Dialog.style.width = s[0]+"px";
	Dialog.style.height = s[1]+"px";
	Dialog.style.top = "calc(50% - "+Math.round(s[1]*0.5)+"px)";
	Dialog.style.left = "calc(50% - "+Math.round(s[0]*0.5)+"px)";
	if(type == "RS"){
		RS_Title[0].value = my.room.title;
		RS_Hidden[0].checked = my.room.config.password != "";
		RS_Password[0].value = my.room.config.password;
		RS_UserLimit[0].value = my.room.config.userLimit;
		RS_Item[0].checked = my.room.config.item;
		RS_Round[0].value = my.room.config.round;
	}
}
function hiderMsg(type){
	if(!type){
		Hider.style.display = "none";
		return;
	}
	Hider.style.display = "";
	HiderStage.innerHTML = this["HD_"+type].innerHTML;
}