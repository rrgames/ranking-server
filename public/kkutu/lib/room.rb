require 'json'

class Room
	attr_accessor :id
	attr_accessor :title
	attr_accessor :config
	attr_accessor :master
	attr_accessor :users
	attr_accessor :game
	attr_accessor :timer
	# config <Hash> 속성
	#	:userLimit	제한 인원
	#	:password	방 암호
	#	:item			아이템 사용 가능 여부
	#	:round			판 라운드 수
	
	# game <Hash> 속성
	#	:activated		게임 진행 여부
	#	:round			진행 판
	#	:turn			현재 턴
	#	:time			판 제한 시간
	#	:subj			제시어
	#	:wordList		사용 단어 내역
	#	:parsing		사전 파싱 중 여부
	#	:tTime			턴 제한 시간
	#	:ttt				사용자가 파싱 요청을 한 시점의 시간
	#	:leftSent		타임아웃 처리를 위해 받아야 할 요청 수
	#	:dir				턴 진행 방향 (1이 오른쪽)
	
	def initialize()
		@id = $rid
		$rid = $rid+1
		@users = Array.new
		@config = {
			:userLimit => 6,
			:password => "",
			:item => true,
			:round => 5
		}
		@game = {
			:activated => false
		}
	end
	def create(masterId)
		@master = masterId
		#$dbObj.query("INSERT INTO `room` (id) VALUES(#{@id});")
	end
	def remove()
		#$dbObj.query("DELETE FROM `room` WHERE `id` = #{@id};")
	end
	def publishData(code = Con::ROOM_UPDATE)
		publish("global", {
			:code => code,
			:roomId => @id,
			:data => toObject
		})
	end
	def publishGameData(code = Con::GAME_UPDATE)
		ul = Array.new
		@users.each do |item|
			ul.push(item.toObject)
		end
		publish(@id, {
			:code => code,
			:roomId => @id,
			:data => {
				:user => ul,
				:game => @game
			}
		})
	end
	def getUserById(id)
		r = nil
		@users.each do |item|
			if item.id == id then
				r = item
				break
			end
		end
		r
	end
	def ready()
		r = true
		@users.each do |item|
			if item.id != @master then
				if item.ready != true then
					r = false
					break
				end
			end
		end
		r
	end
	def startGame()
		@game[:activated] = true
		@game[:round] = 0
		@game[:turn] = 0
		@game[:dir] = 1
		@users.each do |item|
			item.game.start()
		end
	end
	def startRound()
	# 여기서 게임이 끝났는지 판단
		@game[:round] += 1
		if @game[:round] > @config[:round] then
			# 게임이 끝났다는 신호와 동시에 보상을 지급해야 한다.
			# 순위 매기기
			rank = @users.sort do |a, b|
				b.game.score-a.game.score
			end
			ri = 1
			ra = Array.new
			rank.each do |item|
				ra.push(item.game.flush(ri))
				ri += 1
			end
			publish(@id, {
				:code => Con::FINISH,
				:data => ra
			})
			# 결과창을 띄운 뒤 잠시 후 방장이 activated를 false로 한다
		else
			@game[:time] = TOTAL_TIME
			@game[:subj] = getRandomSubj()
			@game[:wordList] = Array.new
			publishGameData(Con::ROUND_START)
		end
	end
	def startTurn()
	# 턴 시작 및 턴 시간 초과를 담당
		time = getTurnTime(@game[:time])
		@game[:leftSent] = 0
		@game[:tTime] = time
		@users.each do |item|
			item.game.toSent = false
			@game[:leftSent] += 1
		end
		@game[:parsing] = false
		publish(@id, {
			:code => Con::TURN_START,
			:turn => @game[:turn],
			:subj => @game[:subj],
			:time => time
		})
		@timer = DateTime.now.strftime("%Q").to_f
	end
	def sendTimeout(from)
		if @timer == nil then
			return
		end
		if from.game.toSent == false then
			td = DateTime.now.strftime("%Q").to_f - @timer
			# 타임아웃 요청이 오면 그 진위성 판단
			if td >= @game[:tTime] then
				if @game[:parsing] == true then
					# 사전 파싱 요청은 했는데 파싱 결과가 지연될 경우
					from.socket.send_text(JSON.generate({
						:code => Con::TURN_START,
						:turn => @game[:turn],
						:subj => @game[:subj],
						:time => 1000
					}))
				else
					from.game.toSent = true
					@game[:leftSent] -= 1
				end
			else
				# 타임아웃이 유효하지 않은 경우
				from.socket.send_text(JSON.generate({
					:code => Con::TURN_START,
					:turn => @game[:turn],
					:subj => @game[:subj],
					:time => td
				}))
			end
		end
		# 현재 턴인 사람을 제외하기 때문에 0이 아니라 1
		if @game[:leftSent] == 1 then
			@users[@game[:turn]].game.boom()
			endTurn(nil, -1)
			publish(@id, {
				:code => Con::TURN_TIMEOUT
			})
		end
	end
	def endTurn(with, item)
		if with != nil then
			if item != -1 then
				@users[@game[:turn]].game.useItem(item)
				case item
					when 3
						playTurn(@game[:dir], false)
					when 4
						@game[:dir] *= -1
					when 5
						playTurn(-1*@game[:dir], false)
				end
			end
			@game[:wordList].push(with)
			dt = @game[:ttt] - @timer
			@game[:time] -= dt
			if @config[:item] then
				if dt <= @game[:tTime]*0.3 then
					# 아이템 지급 - 제한 시간을 70% 이상 남기고 턴을 넘겼을 때
					@users[@game[:turn]].game.getItem(dt%6)
				end
			end
			@game[:subj] = with[-3, 3]
		end
		@timer = nil
		publishGameData()
	end
	def playTurn(dir = @game[:dir], andStart = true)
		@game[:turn] += dir
		if @game[:turn] >= @users.length then
			@game[:turn] = 0
		end
		if @game[:turn] < 0 then
			@game[:turn] = @users.length-1
		end
		if andStart then
			startTurn()
		end
	end
	def toObject()
		ul = Array.new
		@users.each do |item|
			ul.push(item.toObject)
		end
		r = {
			:id => @id,
			:title => @title,
			:master => @master,
			:user => ul,
			:config => @config
		}
		if @game[:activated] then
			r[:game] = {
				:activated => true,
				:round => @game[:round],
				:turn => @game[:turn],
				:time => @game[:time],
				:subj => @game[:subj],
				:wordList => @game[:wordList],
			}
		end
		r
	end
end