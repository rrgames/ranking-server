var wSocket;
WebSocket.prototype.sendObject = function(obj){
	this.send(JSON.stringify(obj));
}

function connectSocketServer(){
	var url = "wss://rrgames.me:12999?name="+my.fb.name;
	wSocket = new WebSocket(url);

	wSocket.onmessage = function(e){
		var msg = JSON.parse(e.data);
		console.info(msg);
		switch(msg.code){
			case WELCOME:
				my.id = msg.id;
				user = {};
				for(var I in msg.clients){
					user[I] = new Client(msg.clients[I].id, msg.clients[I].name);
				}
				room = {};
				for(I in msg.rooms){
					room[I] = new Room(msg.rooms[I]);
				}
				updateRoom();
				playBGM("lobby");
				break;
			case ERROR:
			case WARN:
				alert(msg.text);
				break;
			case USER_CONNECT:
				user[msg.id] = new Client(msg.id, msg.data);
				drawUserList();
				break;
			case USER_DISCONNECT:
				delete user[msg.id];
				drawUserList();
				break;
			case CHAT:
				attachChat(msg);
				break;
			case ROOM_CREATE:
			case ENTER:
				if(my.id == msg.id || msg.code == ROOM_CREATE){
					my.room = new Room(msg);
					enterRoom(msg.roomId);
				}else{
					if(user.hasOwnProperty(msg.id)){
						msg.data = user[msg.id].name;
						attachChat(msg);
					}else{
						console.warn("유저 정보 조회에 실패");
						setTimeout(wSocket.onmessage, 1000, e);
					}
				}
				break;
			case EXIT:
				if(my.id == msg.id){
					my.room = null;
					exitRoom();
				}else{
					msg.data = user[msg.id].name;
					attachChat(msg);
				}
				break;
			case ROOM_UPDATE:
				if(!room[msg.roomId]){
					if(msg.data.hasOwnProperty("id")){
						msg.data.roomId = msg.data.id;
					}
					room[msg.roomId] = new Room(msg.data);
				}else{
					var ro = room[msg.roomId];
					ro.title = msg.data.title;
					ro.game = msg.data.game;
					ro.master = msg.data.master;
					ro.user = msg.data.user;
					ro.config = msg.data.config;
				}
				if(my.room != null){
					if(my.room.id == msg.roomId){
						my.room = room[msg.roomId];
					}
				}
				updateRoom();
				break;
			case ROOM_DELETE:
				delete room[msg.roomId];
				updateRoom();
				break;
			case READY:
				my.room.getUserById(msg.id).ready = msg.data;
				if(my.id == msg.id){
					my.ready = msg.data;
					ReadyBtn.value = my.ready?"준비 취소":"준비";
				}
				updateRoom();
				break;
			case START:
				startGame();
				break;
			case ROUND_START:
				my.room.game = msg.data.game;
				startRound(msg.data.game.round);
				break;
			case TURN_START:
				startTurn(msg);
				break;
			case TURN_TIMEOUT:
				drawTimeout();
				break;
			case GAME_UPDATE:
				updateGame(msg.data);
				break;
			case ACCEPT:
				accept(msg.data);
				break;
			case WRONG_WORD:
				wrong(msg.data);
				break;
			case FINISH:
				finishGame(msg.data);
				break;
			default:
				console.warn("알 수 없는 유형: ", msg);
		}
	}
	wSocket.onopen = function(e){
		console.info("서버 연결 완료");
	}
	wSocket.onclose = function(e){
		alert("서버가 닫혀있어요ㅠㅠ");
	}
}

function send(data){
	var sd = {
		code: CHAT,
		data: data,
		item: -1,
		id: my.id
	};
	if(isGaming){
		if(my.room.user[g.turn].id == my.id){
			var il = g.item.length;
			for(var i=0; i<il; i++){
				// 현재 한 턴에 하나의 아이템만 사용할 수 있다.
				if(g.item[i] == 1){
					sd.item = i;
					break;
				}
			}
		}
	}
	wSocket.sendObject(sd);
}
function requestCreateRoom(name){
	wSocket.sendObject({
		code: ROOM_CREATE,
		data: {
			name: name
		},
		id: my.id
	});
}
function requestEnterRoom(e){
	var o = room[e.target.getAttribute("data-id")];
	wSocket.sendObject({
		code: ENTER,
		data: {
			id: o.id
		},
		id: my.id
	});
}
function requestConfigRoom(){
	wSocket.sendObject({
		code: ROOM_UPDATE,
		data: {
			title: RS_Title[0].value,
			password: RS_Hidden[0].checked?RS_Password[0].value:"",
			userLimit: RS_UserLimit[0].value,
			item: RS_Item[0].checked,
			round: RS_Round[0].value
		}
	});
}
function requestStartRound(){
	wSocket.sendObject({
		code: ROUND_START,
		data: {
			roomId: my.room.id
		}
	});
}
function requestStartTurn(){
	wSocket.sendObject({
		code: TURN_START,
		data: {
			roomId: my.room.id
		}
	});
}
function requestStart(){
	wSocket.sendObject({
		code: TRY_START,
		data: {
			roomId: my.room.id
		}
	});
}
function requestToggleReady(){
	wSocket.sendObject({
		code: READY,
		data: !my.ready,
		id: my.id
	});
}
function requestNextTurn(){
	wSocket.sendObject({
		code: TURN_PLAY,
		data: {
			roomId: my.room.id
		}
	});
}
function requestTurnTimeout(){
	g.timer = -1;
	wSocket.sendObject({
		code: TURN_TIMEOUT,
		data: {
			roomId: my.room.id
		}
	});
}
function requestEndGame(){
	wSocket.sendObject({
		code: TRY_END,
		data: {
			roomId: my.room.id
		}
	});
}
function requestExitRoom(){
	wSocket.sendObject({
		code: EXIT
	});
}