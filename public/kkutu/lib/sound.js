var SC = {
	bgm: new Audio(),
	mute: false,
	sounds: new Array()
}

function playBGM(type){
	if(!SC.bgm.paused){
		SC.bgm.pause();
	}
	switch(type){
		case "lobby":
			SC.bgm.src = "./media/LobbyBGM.mp3";
			break;
		default:
	}
	SC.bgm.id = "sBGM";
	SC.bgm.loop = true;
	SC.bgm.play();
}
function stopBGM(fade){
	if(fade){
		aStopBGM();
	}
}
function playSound(type, id){
	if(SC.mute) return;
	stopSound(id);
	var R = new Audio();
	switch(type){
		case "T0":
		case "T1":
		case "T2":
		case "T3":
		case "T4":
		case "T5":
		case "T6":
		case "T7":
		case "T8":
		case "T9":
		case "T10":
			R.src = "./media/"+type+".mp3";
			break;
		case "W":
			R.src = "./media/hwa.wav";
			break;
		case "WW":
			R.src = "./media/phwa.wav";
			break;
		case "F":
			R.src = "./media/fail.mp3";
			break;
		default:
	}
	R.id = "s"+id;
	R.play();
	SC.sounds.push(R);
}
function stopSound(id){
	id = "s"+id;
	var i = 0;
	var s = SC.sounds[i];
	while(s){
		if(s.id == id){
			s.pause();
			SC.sounds.splice(i, 1);
			break;
		}
		i++;
		s = SC.sounds[i];
	}
}
function updateMute(){
	if(SC.mute){
		SC.bgm.pause();
		SC.sounds.forEach(function(item, index, my){
			item.pause();
		});
		MuteBtn.value = "음악 재생";
	}else{
		if(SC.bgm.paused){
			SC.bgm.play();
		}
		MuteBtn.value = "음소거";
	}
}