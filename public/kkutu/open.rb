require 'em-websocket'
require 'mysql'
require 'json'
require 'uri'
require 'date'
require_relative 'parser'
require_relative 'lib/room'
require_relative 'lib/const'
require_relative 'lib/client'
require_relative 'lib/game'
require_relative 'lib/log'

$rid = 1
$gen = Random.new
$rooms = {}
$clients = {}
$dbObj = Mysql.new("localhost", "root", "dlalsgur", "kkutu")
$wordHit = 0 # DB에서 불러와야 함

TOTAL_TIME = 150000

def send(ws, data)
	ws.send_text(JSON.generate(data))
end
def publish(channel, data)
	json = JSON.generate(data)
	log "#{Time.now} PBL #{data[:code]} #{channel} #{json.bytesize}"
	global = channel == "global"
	$clients.each do |name, value|
		if global || (value.room == channel) then
			value.socket.send_text(json)
		end
	end
end
def getRandomSubj()
	a = "_"+Con::C_C[$gen.rand(0...Con::C_CL)]
	b = Con::C_V[$gen.rand(0...Con::C_VL)]
	a+b
end
def getTurnTime(val)
	# 음향 효과를 위해 턴 제한 시간은 11종류
	# T0			T1		T2			T10
	# 15000	13600	12200	...	1000
	1000+((val/TOTAL_TIME*10).to_i*1400)
end
def getExp(score)
	score
end
def getScore(data)
	data[:bonusP] = 0
	data[:bonusM] = 1-data[:usage]
	# data
	#	:word		해당 단어
	#	:usage	사용 비율 (전체 중, 0~1)
	#	:bonusP	+보너스 (기본 0)
	#	:bonusM	*보너스 (기본 1)
	(10 * data[:word].length * data[:bonusM] + data[:bonusP]).to_i
end
def isValidWord(word, subj, one)
	wdc = word.downcase()
	((word.length >= 4) && (((subj[1, 2] == word[0, 2]) || subj == word[0, 3]) || (one && (subj[2] == word[0])))) && (wdc.gsub(/[^a-z]/, "") == wdc)
end

EM.run do
	puts "안내: 서버 작동 시작"
	clientId = nil

	EM::WebSocket.start({
		:host => "0.0.0.0",
		:port => 12999,
		:secure => true,
		:tls_options => {
			:private_key_file => "/etc/ssl/private/key.pem",
			:cert_chain_file => "/etc/ssl/private/crt.pem"
		}
	}) do |ws|
		ws.onopen do |handshake|
			clientId = handshake.headers["Sec-WebSocket-Key"]
			userName = URI.unescape(handshake.query["name"]).force_encoding("utf-8")
			puts "안내: 클라이언트 의 접속 (#{clientId}, 이름 #{userName})"

			# 사용자 정보 등록
			$clients[clientId] = Client.new(clientId)
			$clients[clientId].name = userName
			$clients[clientId].socket = ws
			eval("ws.define_singleton_method(:client) { $clients['#{clientId}'] }")
			#$dbObj.query("INSERT INTO `user` (id, name) VALUES('#{clientId}', '#{userName}');")
			ca = {}
			ra = {}
			$clients.each do |name, value|
				ca[name] = {
					:id => value.id,
					:name => value.name
				}
			end
			$rooms.each do |name, value|
				ra[name] = value.toObject
			end
			send(ws, {
				:code => Con::WELCOME,
				:id => clientId,
				:clients => ca,
				:rooms => ra
			})
			
			#입장 사실 알리기
			publish("global", {
				:code => Con::USER_CONNECT,
				:id => clientId,
				:data => userName
			})
		end

		ws.onerror do |e|
			puts "오류: #{e.message}"
			log "#{Time.now} ERR #{e.message}"
		end

		ws.onclose do |data|
			puts "안내: 클라이언트 접속 종료 (#{ws.client().id})"
			
			#퇴장 사실 알리기
			#res = $dbObj.query("SELECT `name` FROM `user` WHERE `id` = '#{ws.client().id}';")
			#if res then
			#	res = res.fetch_row()[0].force_encoding("utf-8")
			#end

			ws.client().exit()
			publish("global", {
				:code => Con::USER_DISCONNECT,
				:id => ws.client().id,
				:data => ws.client().name
			})
			#$dbObj.query("DELETE FROM `user` WHERE `id` = '#{ws.client().id}';")
			$clients.delete(ws.client().id)
		end

		ws.onmessage do |msg|
			my = ws.client()
			o = JSON.parse(msg)
			log "#{Time.now} REQ #{o["code"]} #{msg.length}"
			myRoom = $rooms[my.room]
			case o["code"]
				when Con::CHAT
				# 채팅 명령
					# 현재 턴인 사람이 요청을 보냈는지 확인
					needV = my.game.playing && (myRoom.timer != nil)
					if needV then
						needV = needV && myRoom.users[myRoom.game[:turn]].id == my.id
					end
					if needV then
						if o["item"] == 0 then
							my.game.useItem(o["item"])
							publish(my.room, {
								:code => Con::ACCEPT,
								:data => {
									:item => o["item"]
								}
							})
							myRoom.endTurn(nil, o["item"])
							needV = false
						elsif o["item"] == 2 then
							my.game.useItem(o["item"])
							# 데이터베이스에서 유효한 단어를 찾아 입력해줘야 함
							needV = false
						else
							needV = needV && isValidWord(o["data"], myRoom.game[:subj], o["item"] == 1)
						end
					end
					if needV then
						myRoom.game[:parsing] = true
						myRoom.game[:ttt] = DateTime.now.strftime("%Q").to_f
						res = parseWord(o["data"])
						if res == nil then
							publish(my.room, {
								:code => Con::WRONG_WORD,
								:data => o["data"]
							})
						else
							ah = {
								:word => res[:word],
								:mean => res[:mean],
								:score => getScore(res),
								:usage => res[:usage],
								:item => o["item"]
							}
							my.game.accept(ah)
							publish(my.room, {
								:code => Con::ACCEPT,
								:data => ah
							})
							myRoom.endTurn(o["data"], o["item"])
						end
						myRoom.game[:parsing] = false
					else
						if o["data"] then
							publish(my.room, {
								:code => Con::CHAT,
								:data => o["data"],
								:client => {
									:id => my.id,
									:name => my.name
								}
							})
						end
					end
				when Con::ROOM_CREATE
				# 방 생성
					r = Room.new()
					$rooms[r.id] = r
					r.title = o["data"]["name"]
					r.create(my.id)
					my.enter(r)
					send(ws, {
						:code => Con::ROOM_CREATE,
						:roomId => r.id,
						:title => r.title
					})
					r.publishData()
				when Con::ROOM_UPDATE
				# 방 정보 변경
					r = $rooms[my.room]
					r.title = o["data"]["title"]
					r.config[:password] = o["data"]["password"]
					r.config[:item] = o["data"]["item"]
					r.config[:round] = o["data"]["round"].to_i
					r.config[:userLimit] = o["data"]["userLimit"].to_i
					r.publishData()
				when Con::ENTER
				# 방에 입장
					r = $rooms[o["data"]["id"]]
					if r.game[:activated] then
						send(ws, {
							:code => Con::WARN,
							:text => Msg::ALREADY_ACTIVATED
						})
					else
						my.enter(r)
						publish(r.id, {
							:code => Con::ENTER,
							:id => my.id,
							:roomId => r.id
						})
						r.publishData()
					end
				when Con::READY
				# 준비
					my.ready = o["data"]
					$rooms[my.room].getUserById(my.id).ready = my.ready
					publish(my.room, {
						:code => Con::READY,
						:id => my.id,
						:data => my.ready
					})
				when Con::TRY_START
				# 시작 요청 (방장 권한)
					if my.id == myRoom.master then
						if myRoom.ready() then
							myRoom.startGame()
							publish(my.room, {
								:code => Con::START
							})
							myRoom.publishData()
						else
							send(ws, {
								:code => Con::WARN,
								:text => Msg::NOT_YET_READIED
							})
						end
					end
				when Con::TRY_END
				# 종료 공지 요청 (방장 권한)
					if my.id == myRoom.master then
						myRoom.game[:activated] = false
					end
				when Con::ROUND_START
				# 라운드 시작 요청 (방장 권한)
					if my.id == myRoom.master then
						myRoom.startRound()
					end
				when Con::TURN_START
				# 턴 시작 요청 - 제한 시간 시작 등 (방장 권한)
					if my.id == myRoom.master then
						myRoom.startTurn()
					end
				when Con::TURN_PLAY
					if my.id == myRoom.master then
						myRoom.playTurn()
					end
				when Con::TURN_TIMEOUT
					if myRoom.users[myRoom.game[:turn]].id != my.id then
						myRoom.sendTimeout(my)
					end
				when Con::EXIT
				# 방에서 퇴장
					my.exit()
				else
					puts "경고: 알 수 없는 유형 ("+o["code"]+")"
			end
		end
	end
end