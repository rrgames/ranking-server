require 'open-uri'

def latinize(arg)
	arg.tr("ÀÁÂÃÄÅàáâãäåĀāĂăĄąÇçĆćĈĉĊċČčÐðĎďĐđÈÉÊËèéêëĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħÌÍÎÏìíîïĨĩĪīĬĭĮįİıĴĵĶķĸĹĺĻļĽľĿŀŁłÑñŃńŅņŇňŉŊŋÒÓÔÕÖØòóôõöøŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšſŢţŤťŦŧÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲųŴŵÝýÿŶŷŸŹźŻżŽž", "AAAAAAaaaaaaAaAaAaCcCcCcCcCcDdDdDdEEEEeeeeEeEeEeEeEeGgGgGgGgHhHhIIIIiiiiIiIiIiIiIiJjKkkLlLlLlLlLlNnNnNnNnnNnOOOOOOooooooOoOoOoRrRrRrSsSsSsSssTtTtTtUUUUuuuuUuUuUuUuUuUuWwYyyYyYZzZzZz")
end
def parseWord(word)
	r = "_"
	open("http://m.dic.daum.net/search.do?q="+word+"&dic=eng") do |file|
		data = file.read
		
		sr = data.split("class=\"inner_tit\"")[1];
		if sr == nil then
			r = "X"
		else
			sr = latinize(sr.slice((sr.index(">")+1)...(sr.index("</span>"))).gsub(/([·-]|\s)/, "").downcase())
			if word == sr then
				data = data.split("<div class=\"wrap_meaning\" >")[1]
				data = data.slice(1...(data.index("</div>"))).gsub(/&nbsp;/, " ")
				data = data.gsub(/<("[^"]*"|'[^']*'|[^'">])*>/, "")
				r = data
			else
				r = "N"
			end
		end
	end
	# 전체 단어 사용 빈도:		서버를 켤 때 전역 변수($wordHit)로 저장해 뒀다가
	# 										매 시행마다 1만큼 더하면서 DB에 저장한다.
	# 해당 단어의 사용 빈도:	매 시행마다 1만큼 더하도록 질의를 한다.
	#										빈도는 뜻을 질의할 때 같이 구한다.

	# 반환값 r
	#	:word		단어
	#	:mean		뜻
	#	:usage		사용 비율 (<해당 단어의 사용 빈도>/$wordHit)
	if r.length > 1 then
		{
			:word => word,
			:mean => r,
			:usage => 0.1
		}
	else
		nil
	end
end