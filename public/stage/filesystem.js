/*
 EEEEE           EEEEE
E  _  E         E  _  E
E |_| E_________E |_| E  스테이지 디자인 툴
E         O  O        E     Stage Design Tool
E                     E
E   |_____________|   E  ☞파일 입출력 자바스크립트
E                     E
E                     E  버전 0.9.6.3
 EEEEEEEEEEEEEEEEEEEEE              RR Games
*/
var SPACE = "　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　";
var stageCache;
var viewerWidth = 10;
var viewerHeight = 0;

ViewerScroller.style.display = "none";
function overviewFiles(list){
	var ll = list.length;
	Viewer.innerHTML = "";
	stageCache = new Object();
	viewerWidth = 10;
	viewerHeight = 0;
	for(var i=0; i<ll; i++){
		readFile(list[i], function(e){
			addView(new DOMParser().parseFromString(e.currentTarget.result, "text/xml"));
		});
	}
}
function addView(xml){
	var X = xml.getElementsByTagName("stage")[0];
	stageCache[X.get("name").innerHTML] = xml;

	var D = document.createElement("div");
	D.className = "ViewPort";
	var ml = X.get("map").innerHTML.split(",");
	var size = [X.get("width").innerHTML, X.get("height").innerHTML];
	var st = Array.create2DArray(size[0], size[1], 0);
	var R = "<div class='ViewStage' style='float: left; width:"+(14*size[0])+"px; height: "+(14*size[1])+"px;'>";
	viewerWidth += 14*size[0]+6;
	viewerHeight = Math.max(viewerHeight, 14*size[1]+30);
	Viewer.style.width = viewerWidth+"px";
	Viewer.style.height = viewerHeight+"px";
	var ml = X.get("map").innerHTML.split("");
	var mll = ml.length;
	var rc = 0;
	for(i=0; i<mll; i++){
		var cv = (ml[i]=="0")?0:ml[i];
		if(cv == "N"){
			do{
				i++;
				cv += ml[i];
			}while(!isNaN(Number(ml[i+1])));
		}
		st[Math.floor(rc/size[0])][rc%size[0]] = cv;
		rc++;
	}
	for(i=0; i<size[1]; i++){
		for(var j=0; j<size[0]; j++){
			var v = st[i][j];
			R += "<div class='ViewBlock' style='background-color: "+((v<=0)?"#DDD":COLOR[v.charAt()])+";'>";
			if(v.constructor == String){
				if(v.charAt() == "N"){
					R += v.slice(1);
				}
			}
			R += "</div>";
		}
	}
	R += "</div>";
	D.innerHTML = "<div class='ViewTitle' style='width: "+(14*size[0])+"px;'>"+X.get("name").innerHTML+"</div><br />"+R;
	D.onclick = function(e){
		var ID = e.currentTarget.getElementsByClassName("ViewTitle")[0].innerHTML;
		attach(stageCache[ID]);
	}
	Viewer.appendChild(D);
}
function readFile(file,loadHandler){
	var reader = new FileReader();
	reader.onload = loadHandler || function(e){
		attach(new DOMParser().parseFromString(reader.result, "text/xml"));
	}
	reader.readAsText(file, "utf-8");
}
function saveFile(send){
	var myData = getSaveData(send);
	if(send){
		if(StageName.value.split("-").length != 2){
			if(!confirm("스테이지 식별자 ("+StageName.value+")가 올바르지 않습니다.\n계속할 경우 서버에 저장은 되지만, 스테이지 목록을 조회할 때 조회되지 않습니다.\n그래도 계속하시겠습니까?")){
				return;
			}
		}
		flashContent.fromJS("saveXML", {id: StageName.value, xml: myData});
	}else{
		Popup.style.display = "";
		PopupText.innerText = myData;
	}
}
function getSaveData(zip){
	var R = new Array();
	
	function s(num){
		return zip?"":SPACE.substr(0, num*2);
	}
	R.push("<stage>");
		R.push(s(1)+"<name>"+StageName.value+"</name>");
		R.push(s(1)+"<grade>");
			R.push(s(2)+"<L1>"+Grade1.value+"</L1>");
			R.push(s(2)+"<L2>"+Grade2.value+"</L2>");
			R.push(s(2)+"<L3>"+Grade3.value+"</L3>");
		R.push(s(1)+"</grade>");
		R.push(s(1)+"<rule>");
			R.push(s(2)+"<gravity>"+Gravity.checked+"</gravity>");
			R.push(s(2)+"<infinite>"+Infinite.checked+"</infinite>");
			R.push(s(2)+"<colorflag>"+NumColor.value+"</colorflag>");
		R.push(s(1)+"</rule>");
		R.push(s(1)+"<missions>");
	var cm = CurrentMission.options;
	for(var i=0; i<cm.length; i++){
		var mv = cm[i].value;
			R.push(s(2)+"<mission>"+mv+"</mission>");
	}
		R.push(s(1)+"</missions>");
		R.push(s(1)+"<width>"+mapSize[0]+"</width>");
		R.push(s(1)+"<height>"+mapSize[1]+"</height>");
	var arr = "";
	for(i=0; i<stage.length; i++){
		arr += stage[i].join("").toString();
	}
		R.push(s(1)+"<map>"+arr+"</map>");
		R.push(s(1)+"<description>"+TxtDesc.value+"</description>");
	R.push("</stage>");
	
	var RS;
	if(zip){
		RS = R.join("").toString();
	}else{
		RS = R.join("\n").toString();
	}
	
	return RS;
}
Element.prototype.get = function(name){
	return this.getElementsByTagName(name)[0] || this.getAttribute(name);
}
function attach(xml){
	init();

	var X = xml.getElementsByTagName("stage")[0];
	
	if(isOld.checked){
		StageName.value = X.get("name");
		Grade1.value = X.get("grade").get("L1").innerHTML;
		Grade2.value = X.get("grade").get("L2").innerHTML;
		Grade3.value = X.get("grade").get("L3").innerHTML;
		Gravity.checked = eval(X.get("rule").get("gravity").innerHTML);
		Infinite.checked = eval(X.get("rule").get("infinite").innerHTML);
		NumColor.value = eval(X.get("rule").get("numcolor").innerHTML);
		var ml = X.get("mission").getElementsByTagName("item");
		
		for(var i=0; i<ml.length; i++){
			var type = ml[i].get("type");
			CurrentMission.add(newOption("["+type+"] "+getMissionUIText(type, ml[i].innerHTML.split(",")), type+ml[i].innerHTML));
		}
		
		MapSizeX.value = X.get("map").get("width");
		MapSizeY.value = X.get("map").get("height");
		resizeMap(MapSizeX.value, MapSizeY.value);
		ml = X.get("map").innerHTML.split(",");
		for(i=0; i<mapSize[0]*mapSize[1]; i++){
			stage[Math.floor(i/mapSize[0])][i%mapSize[0]] = (ml[i]=="0")?0:ml[i];
		}
		TxtDesc.value = X.get("description").innerHTML;
	}else{
		StageName.value = X.get("name").innerHTML;
		Grade1.value = X.get("grade").get("L1").innerHTML;
		Grade2.value = X.get("grade").get("L2").innerHTML;
		Grade3.value = X.get("grade").get("L3").innerHTML;
		Gravity.checked = eval(X.get("rule").get("gravity").innerHTML);
		Infinite.checked = eval(X.get("rule").get("infinite").innerHTML);
		NumColor.value = eval(X.get("rule").get("colorflag").innerHTML);
		var ml = X.get("missions").getElementsByTagName("mission");
		
		for(var i=0; i<ml.length; i++){
			var text = ml[i].innerHTML;
			var type = text.substr(0,2);
			CurrentMission.add(newOption("["+type+"] "+getMissionUIText(type, text.slice(2).split(",")), text));
		}
		
		MapSizeX.value = X.get("width").innerHTML;
		MapSizeY.value = X.get("height").innerHTML;
		resizeMap(MapSizeX.value, MapSizeY.value);
		ml = X.get("map").innerHTML.split("");
		var mll = ml.length;
		var rc = 0;
		for(i=0; i<mll; i++){
			var cv = (ml[i]=="0")?0:ml[i];
			if(cv == "N"){
				do{
					i++;
					cv += ml[i];
				}while(!isNaN(Number(ml[i+1])));
			}
			stage[Math.floor(rc/mapSize[0])][rc%mapSize[0]] = cv;
			rc++;
		}
		TxtDesc.value = X.get("description").innerHTML;
	}
	
	draw();
}