/*
 EEEEE           EEEEE
E  _  E         E  _  E
E |_| E_________E |_| E  스테이지 디자인 툴
E         O  O        E     Stage Design Tool
E                     E
E   |_____________|   E  ☞테스트 플레이 자바스크립트
E                     E
E                     E  버전 0.9.2.1
 EEEEEEEEEEEEEEEEEEEEE              RR Games
*/
/*
아이템

-	가로 세로			누적 파괴 수 40개마다
-	3x3 폭탄			같은 색 블럭을 연달아 4회
-	퀸						한꺼번에 30개
-	같은 색 파괴		구매
-	다이너마이트		구매

-	(사이버)	핑		은화 ⓟ
-	(캐시)		쪼		금화 ⓙ

*/
var _score;
var score;
var gameStage;
var options;
var achieve;
var clear;

var popList;
var falling;

function gameInit(){
	if(TestBtn.value == "테스트 종료"){
		switchEnable(true);
		draw();
		return;
	}
	score = 0;
	_score = 0;
	gameStage = Array.create2DArray(mapSize[0],mapSize[1],0);
	options = {
		grade: [Number(Grade1.value), Number(Grade2.value), Number(Grade3.value)],
		gravity: Gravity.checked,
		infinite: Infinite.checked,
		enableColor: getEnableColorList(NumColor.value),
		mission: parseMission()
	}
	var mapS = stage.toString().split(",");
	for(var i=0; i<mapSize[0]*mapSize[1]; i++){
		if(mapS[i] == "S"){
			mapS[i] = getRandomColor();
		}
		gameStage[Math.floor(i/mapSize[0])][i%mapSize[0]] = (mapS[i]=="0")?0:mapS[i];
	}
	achieve = {
		CB: new Object(),
		DB: Array.create2DArray(mapSize[0], mapSize[1], 0),
		ML: 0,
		PM: new Object(),
		TL: 0
	}
	for(i in COLOR){
		achieve.CB[i] = 0;
		achieve.PM[i] = 0;
	}
	achieve.PM.NTemp = 0;
	
	switchEnable(false);
	draw(true);
}
function switchEnable(bool){
	[
		LoadBtn, MapSizeX, MapSizeY, MapResizeBtn,
		Gravity, Infinite, NumColor,
		MissionAddBtn, MissionDelBtn, MissionApplyBtn
	].forEach(function(obj){
		obj.disabled = !bool;
	});
	TestBtn.value = bool?"테스트":"테스트 종료";
	
	if(bool){
		clearInterval(options.update);
	}else{
		options.update = setInterval(update, 50);
	}
}
function getEnableColorList(text){
	var R = new Array();
	text.split("").forEach(function(item, index, my){
		if(item == "1"){
			R.push(COLOR_LIST[index]);
		}
	});
	
	return R;
}
function update(){
	var deltaScore = _score-score;
	if(Math.abs(deltaScore) < 1){
		score = _score;
	}else{
		score += deltaScore*0.4;
	}
	achieve.TL += 0.05;
	updateBottom(true);
}
function isStageClean(){
	var R = true;
	for(var i=0; i<mapSize[1]; i++){
		for(var j=0; j<mapSize[0]; j++){
			var ct = gameStage[i][j];
			if(ct.constructor != Number && ct != "W"){
				R = false;
				break;
			}
		}
		if(!R) break;
	}
	return R;
}
function getCurrentMissionAsHTML(){
	var R = "";
	clear = true;
	
	for(var i=0; i<options.mission.length; i++){
		var T = options.mission[i];
		var ok;
		R += "<label class='MissionTitle'>"+getMissionUIText(T.type, T.arg)+"</label><br />└&nbsp;";
		var adv = "";
		switch(T.type){
			case "AC":
				adv = "화면 비우기";
				ok = isStageClean();
				break;
			case "CB":
				adv = achieve.CB[T.arg[0]]+"개 파괴";
				ok = achieve.CB[T.arg[0]] >= T.arg[1];
				break;
			case "DB":
				adv = achieve.DB[T.arg[1]][T.arg[0]]+"회 파괴";
				ok = achieve.DB[T.arg[1]][T.arg[0]] >= 1;
				break;
			case "ML":
				adv = achieve.ML+"턴째";
				ok = achieve.ML <= T.arg[0];
				break;
			case "PM":
				adv = "한꺼번에 "+achieve.PM[T.arg[0]]+"개 파괴";
				ok = achieve.PM[T.arg[0]] >= T.arg[1];
				break;
			case "SC":
				adv = _score+"점";
				ok = _score >= T.arg[0];
				break;
			case "TL":
				adv = Math.floor(achieve.TL)+"초 경과";
				ok = achieve.TL < T.arg[0];
				break;
			default:
		}
		if(ok){
			adv = "<label style='color: #007700; font-weight: bold;'>[완료]</label> "+adv;
		}
		clear = clear && ok;
		R += adv+"<br />";
	}
	
	return R;
}
function parseMission(){
	var R = new Array();
	var ml = CurrentMission.options;
	for(var i=0; i<ml.length; i++){
		var s = ml[i].get("value");
		R.push({
			type: s.substr(0,2),
			arg: s.slice(2).split(",")
		});
	}
	return R;
}
function gameClick(e){
	if(falling) return;
	var pos = e.currentTarget.innerText.replace(/\s/g,"").split(",");		// [X, Y]
	var color = gameStage[pos[1]][pos[0]];
	if(color <= 0) return;
	color = color.charAt();
	if(color == "W") return;
	if(color == "N") return;
	popList = [pos.toString()];
	achieve.Nuisance = new Array();
	executePop(pos,color);
	var pl = popList.length;
	if(pl < 2) return;
	falling = true;

	var nextPos = achieve.Nuisance;
	var nl = nextPos.length;
	for(var i=0; i<nl; i++){
		var gn = gameStage[nextPos[i][1]][nextPos[i][0]];
		if(gn.constructor != String) continue;
		var chp = gn.slice(1)-1;
		if(chp > 0){
			gameStage[nextPos[i][1]][nextPos[i][0]] = "N"+chp;
		}else{
			gameStage[nextPos[i][1]][nextPos[i][0]] = 0;
			achieve.CB.N++;
			achieve.DB[nextPos[i][1]][nextPos[i][0]]++;
			achieve.PM.NTemp++;
			achieve.PM.N = Math.max(achieve.PM.N, achieve.PM.NTemp);
		}
	}
	achieve.PM.NTemp = 0;
	achieve.ML++;
	for(i=0; i<pl; i++){
		var myl = popList[i].split(",");
		achieve.CB[gameStage[myl[1]][myl[0]].charAt()]++;
		achieve.DB[myl[1]][myl[0]]++;
	}
	achieve.PM[gameStage[myl[1]][myl[0]].charAt()] = Math.max(pl, achieve.PM[gameStage[myl[1]][myl[0]].charAt()]);
	
	addScore(Math.pow(popList.length,2)*10);
	animatePop();
}
function addScore(val){
	_score += val;
}
function animatePop(){
	var pl = popList.length;
	var O;
	for(var i=0; i<pl; i++){
		var myl = popList[i].split(",");
		O = this["Block"+myl[0]+"_"+myl[1]];
		O.style.opacity -= 0.1;
	}
	if(O.style.opacity > 0.11){
		setTimeout(animatePop, 70);
	}else{
		realPop();
	}
}
function realPop(){
	falling = false;
	for(var i=0; i<popList.length; i++){
		var myl = popList[i].split(",");
		gameStage[myl[1]][myl[0]] = 0;
	}
	draw(true);
	if(options.gravity){
		gravityDown();
	}else{
		checkClear();
	}
}
function gravityDown(){
	falling = false;
	var zeros = new Array();
	for(var i=0; i<mapSize[1]; i++){
		for(var j=0; j<mapSize[0]; j++){
			if(gameStage[i][j] == 0){
				falling = true;
				zeros.push([j,i]);
			}
		}
	}
	if(zeros.length > 0){
		var as = true;
		for(j=zeros[0][1]; j>0; j--){
			if(gameStage[j][zeros[0][0]] == "W"){
				continue;
			}
			if(gameStage[j-1][zeros[0][0]] == "W"){
				for(i=j-2; i>=0; i--){
					if(gameStage[i][zeros[0][0]] != "W") break;
				}
				if(i == -1){
					gameStage[j][zeros[0][0]] = options.infinite?getRandomColor():(-1);
					as = false;
				}else{
					gameStage[j][zeros[0][0]] = gameStage[i][zeros[0][0]];
				}
				continue;
			}
			gameStage[j][zeros[0][0]] = gameStage[j-1][zeros[0][0]];
		}
		if(as) gameStage[0][zeros[0][0]] = options.infinite?getRandomColor():(-1);
	}
	draw(true);
	if(falling){
		setTimeout(gravityDown, 70);
	}else{
		checkClear();
	}
}
function getGrade(val){
	var R;
	for(var i=0; i<options.grade.length; i++){
		if(val < options.grade[i]){
			break;
		}
	}
	return ["","★☆☆","★★☆","★★★"][i];
}
function checkClear(){
	if(clear){
		alert("클리어 조건을 모두 만족했으므로 게임을 종료합니다.\n점수: "+_score+"점 ["+getGrade(_score)+"]");
		gameInit();
	}
}
function getRandomColor(){
	return options.enableColor.chooseOne();
}
function executePop(pos,color){
	var nextPos = new Array();
	if(pos[0]>0){
		nextPos.push([pos[0]-1,pos[1]]);
	}
	if(pos[0]<mapSize[0]-1){
		nextPos.push([Number(pos[0])+1,pos[1]]);
	}
	if(pos[1]>0){
		nextPos.push([pos[0],pos[1]-1]);
	}
	if(pos[1]<mapSize[1]-1){
		nextPos.push([pos[0],Number(pos[1])+1]);
	}
	for(var i=0; i<nextPos.length; i++){
		var pid = nextPos[i].toString();
		if(popList.indexOf(pid) != -1) continue;
		var gn = gameStage[nextPos[i][1]][nextPos[i][0]];
		if(gn == undefined) continue;
		if(gn.constructor != String) continue;
		if(gn.charAt() == "N"){
			achieve.Nuisance.push(nextPos[i]);
		}
		if(gameStage[nextPos[i][1]][nextPos[i][0]] == color){
			popList.push(pid);
			executePop(nextPos[i], color);
		}
	}
}