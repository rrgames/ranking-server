/*
 EEEEE           EEEEE
E  _  E         E  _  E
E |_| E_________E |_| E  스테이지 디자인 툴
E         O  O        E     Stage Design Tool
E                     E
E   |_____________|   E  ☞주 자바스크립트
E                     E
E                     E  버전 0.9.3
 EEEEEEEEEEEEEEEEEEEEE              RR Games
*/
var COLOR = {
	R: "#FF0000",
	Y: "#FFFF00",
	G: "#009900",
	B: "#0000FF",
	P: "#990099",
	N: "#000000",
	W: "#444444",
	S: "rgba(255,0,0,0.3)",
	E: "rgba(123,123,123,0.5)",
}
var COLOR_DESC = {
	R: "빨강",
	Y: "노랑",
	G: "초록",
	B: "파랑",
	P: "보라",
	N: "방해",
	W: "벽",
	S: "무작위",
	E: "공백",
}
var COLOR_LIST = ["R","Y","G","B","P"];
var MISSION_TYPE = ["AC","CB","DB","ML","PM","SC","TL"];
var MISSION_ACT = {
	AC: {
		arg: function(){
			return [];
		},
		desc: function(arg){
			return "화면 비우기";
		}
	},
	CB: {
		arg: function(){
			return [CB_Color[0].value, CB_ColorQ[0].value];
		},
		desc: function(arg){
			return COLOR_DESC[arg[0]]+" 블럭 "+arg[1]+"개 제거";
		}
	},
	DB: {
		arg: function(){
			return [DB_X[0].value, DB_Y[0].value];
		},
		desc: function(arg){
			return "(X: "+arg[0]+", Y: "+arg[1]+")에 있는 블럭 제거";
		}
	},
	ML: {
		arg: function(){
			return [ML_Move[0].value];
		},
		desc: function(arg){
			return "제한 턴 "+arg[0]+"회";
		}
	},
	PM: {
		arg: function(){
			return [PM_Color[0].value, PM_ColorQ[0].value];
		},
		desc: function(arg){
			return COLOR_DESC[arg[0]]+" 블럭 한꺼번에 "+arg[1]+"개 제거";
		}
	},
	SC: {
		arg: function(){
			return [SC_Score[0].value];
		},
		desc: function(arg){
			return arg[0]+"점 달성";
		}
	},
	TL: {
		arg: function(){
			return [TL_Time[0].value];
		},
		desc: function(arg){
			return "제한 시간 "+arg[0]+"초";
		}
	}
}

var stage;
var picked = "R";
var mapSize = [9,9];

function requestTest(){
	gameInit();
}
function requestSave(send){
	saveFile(send);
}
function requestLoad(send){
	if(send){
		flashContent.fromJS("loadXML", {id: StageName.value});
	}else{
		StageBrowser.click();
	}
}
StageBrowser.onchange = function(e){
	var A = StageBrowser.files;
	ViewerScroller.style.display = (A.length<=1)?"none":"";
	if(A.length <= 1){
		readFile(A[0]);
	}else{
		overviewFiles(A);
	}
}
function requestAddMission(){
	CurrentMission.add(newOption("<!-- 선택하세요 -->"));
	CurrentMission.selectedIndex = CurrentMission.length-1;
}
function requestDelMission(){
	if(CurrentMission.selectedIndex<0) return;
	CurrentMission.remove(CurrentMission.selectedIndex);
}
function requestApplyMission(){
	var R = MISSION_TYPE[MissionTrigger.selectedIndex-1];
	var W = "["+R+"] ";
	var arg = MISSION_ACT[R].arg();
	W += getMissionUIText(R,arg);
	R += arg.toString();
	CurrentMission.selectedOptions[0].innerText = W;
	CurrentMission.selectedOptions[0].value = R;
}
function getMissionUIText(type, arg){
	var R = MISSION_ACT[type].desc(arg);
	return R;
}
function newOption(text, value){
	var R = document.createElement("option");
	R.text = text;
	R.value = value;
	return R;
}
CurrentMission.onchange = function(e){
	MissionTrigger.selectedIndex = MISSION_TYPE.indexOf(e.currentTarget.value.slice(0,2))+1;
	MissionTrigger.onchange({currentTarget: MissionTrigger});
}
Infinite.onchange = function(e){
	NumColor.disabled = !e.currentTarget.checked;
}
MissionTrigger.onchange = function(e){
	var O = "";
	switch(e.currentTarget.value){
		case "":
			break;
		default:
			O = document.getElementById("MissionType_"+e.currentTarget.value).innerHTML;
	}
	MissionSetter.innerHTML = O;
}
Array.create2DArray = function(width,height,fillWith){
	var R = new Array();
	for(var i=0; i<height; i++){
		var r = new Array();
		for(var j=0; j<width; j++){
			r.push(fillWith);
		}
		R.push(r);
	}
	return R;
}
function init(){
	Palette.innerHTML = "";
	CurrentMission.innerHTML = "";

	for(var I in COLOR){
		var N = document.createElement("div");
		N.innerText = "_"+I;
		N.className = "FL Plt";
		N.style.color = COLOR[I];
		N.style.backgroundColor = COLOR[I];
		N.onclick = function(e){
			if(TestBtn.value == "테스트 종료") return;
			var O = e.currentTarget;
			picked = O.innerText.charAt(1);
			PickedText.innerText = COLOR_DESC[picked];
			NuisanceLevelPanel.style.display = (picked == "N")?"":"none";
			draw();
		}
		Palette.appendChild(N);
	}
	stage = Array.create2DArray(mapSize[0],mapSize[1],0);
}
init();
Array.prototype.chooseOne = function(){
	return this[Math.floor(Math.random()*this.length)];
}
function resizeMap(width,height){
	minSize = [Math.min(width,mapSize[0]), Math.min(height,mapSize[1])];
	mapSize = [width,height];
	newStage = Array.create2DArray(width,height,0);
	for(var i=0; i<minSize[1]; i++){
		for(var j=0; j<minSize[0]; j++){
			newStage[i][j] = stage[i][j];
		}
	}
	stage = newStage;
	Canvas.style.width = (40*width)+"px";
	Canvas.style.height = (40*height)+"px";
	draw();
}
function draw(game){
	var st = game?gameStage:stage;

	Canvas.innerHTML = "";
	Picked.style.backgroundColor = COLOR[picked];
	for(var i=0; i<mapSize[1]; i++){			// i는 Y
		for(var j=0; j<mapSize[0]; j++){		// j는 X
			var N = document.createElement("div");
			var sc = st[i][j];
			if(sc) sc = sc.charAt();
			N.className = "Block";
			N.id = "Block"+j+"_"+i;
			N.innerHTML = "<div class='BlockBG' style='background-image: url(image/Block_"+((st[i][j]<=0)?"0":sc)+".png); background-size: 38px;'>"+j+","+i+"</div>";
			N.style.color = (st[i][j]<=0)?"#DDD":COLOR[sc];
			if(st[i][j].constructor == String){
				if(sc == "N"){
					N.innerHTML = "<div class='BlockBG' style='background-image: url(image/Block_"+((st[i][j]<=0)?"0":sc)+".png); background-size: 38px;'><label style='font-size: 9px; color: #000;'>"+j+","+i+",</label><br /><label style='font-size: 11px;'>HP</label>"+st[i][j].slice(1)+"</div>";
					N.style.color = "#FFFFFF";
				}
				if(sc == "W"){
					N.style.color = "#FFFFFF";
				}
				if(sc == "S"){
					N.innerHTML = "<div style='text-align: center; color: #300;'>"+j+","+i+"</div>,???";
					N.style.backgroundColor = "rgba(255, 0, 0, 0.3)";
				}
			}
			N.style.opacity = 1;
			N.onclick = game?gameClick:function(e){
				var O = e.currentTarget;
				var S = O.innerText.replace(/\s/g,"").split(",");	// [X, Y];
				st[S[1]][S[0]] = (picked=="E")?0:picked;
				if(picked == "N"){
					st[S[1]][S[0]] += NuisanceLevel.value;
				}
				draw();
			}
			Canvas.appendChild(N);
		}
	}
	
	updateBottom(game);
}
function updateBottom(game){
	var R = "";
	
	R += "["+(game?"테스트":"편집")+"]&nbsp;";
	if(game){
		R += Math.floor(score)+"점";
		Quest.innerHTML = getCurrentMissionAsHTML();
	}
	
	Bottom.innerHTML = R;
}